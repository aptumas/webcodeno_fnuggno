<?php

/**
 * This script updates the version number in all locations, including docblocks
 *
 * Update the version number in config.ini *before* you run this script!
 *
 * No need to edit below this line
 *
 * @version    1.3.0
 * @link       http://webcode.no
 * @since      1.0.0
 * @package    Webcodeno_Base/bin
 * @author     James Inglis <james@webcode.no>
 */

if (php_sapi_name() === 'cli') {
    $plugin_directory = dirname(dirname(__FILE__));

    $exclude_directories = array('.git', '.idea', 'node_modules', 'bin', 'base');

    mb_internal_encoding('UTF-8');

    $config_array = parse_ini_file(dirname(dirname(__FILE__)) . '/config/config.ini', true);

    if ($config_array === false) {
        throw new Exception('Unable to load config file.');
    }

    $new_values = array(
        'plugin_version' => $config_array['meta']['version'],
        'version' => $config_array['meta']['version'],
        'readme' => '= ' . $config_array['meta']['version'] . ' (' . date('Y-m-d') . ") =\n* ",
    );

    $existing_values = array(
        'plugin_version' => array(
            'regex' => '\* Version: +(\d+\.\d+\.\d+)',
            'replace_with' => '* Version:              %s',
            'directory_match' => '*',
        ),
        'version' => array(
            'regex' => '\* @version +(\d+\.\d+\.\d+)',
            'replace_with' => '* @version    %s',
            'directory_match' => '*',
        ),
        'readme' => array(
            'regex' => '== Changelog ==',
            'replace_with' => "== Changelog ==\n\n%s",
            'directory_match' => '*',
        ),
    );

    $Directory = new RecursiveDirectoryIterator($plugin_directory);
    $objects = new RecursiveIteratorIterator($Directory);

    // Loop through all the files in all the directories
    foreach ($objects as $name => $object) {
        /* @var SplFileInfo $object */
        $path_name = $object->getPath();
        $relative_directory = preg_replace('/^\//', '', str_replace($plugin_directory, '', $path_name));

        // Check whether we need to exclude this path
        $exclude_directory = false;
        foreach ($exclude_directories as $directory) {
            if (preg_match('/^' . $directory . '/', $relative_directory) === 1) {
                $exclude_directory = true;
                break;
            }
        }

        if ($exclude_directory) {
            continue;
        }

        if ($object->isFile() && $object->isWritable()) {
            if ($object->isFile() && $object->isWritable() && preg_match('/^\./', $object->getFilename()) === 0) {
                $file = $object->openFile('r');

                if ($file->getSize() > 0) {
                    $contents = $file->fread($file->getSize());

                    // String replaces on the contents of the file
                    foreach ($existing_values as $key => $existing_value) {
                        if ($existing_value['directory_match'] === '*' || $existing_value['directory_match'] === $relative_directory) {
                            $new_value = sprintf($existing_value['replace_with'], $new_values[$key]);
                            $contents = preg_replace('/' . $existing_value['regex'] . '/', $new_value, $contents);
                        }
                    }

                    $file = $object->openFile('w');
                    $file->fwrite($contents);
                }

                echo 'Updating ' . preg_replace('/^\//', '', $relative_directory . '/' . $object->getFilename()) . PHP_EOL;
            }
        }
    }
}