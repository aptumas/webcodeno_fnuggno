<?php

/**
 * This script initialises the plugin, renaming files, class names and meta information
 *
 * Set up your config.ini *before* you run this script!
 *
 * No need to edit below this line
 *
 * @version    1.3.0
 * @link       http://webcode.no
 * @since      1.0.0
 * @package    Webcodeno_Base/bin
 * @author     James Inglis <james@webcode.no>
 */

$config_array = parse_ini_file(dirname(dirname(__FILE__)) . '/config/config.ini', true);

if ($config_array === false) {
    throw new Exception('Unable to load config file.');
}

$new_values = array(
    'plugin_slug' => $config_array['meta']['name'],
    'plugin_name' => $config_array['meta']['class_name'],
    'plugin_title' => $config_array['meta']['title'],
    'plugin_desc' => $config_array['meta']['description'],
    'url' => $config_array['meta']['author_url'],
    'author_name' => $config_array['meta']['author_name'],
    'author_email' => $config_array['meta']['author_email'],
);

if (php_sapi_name() === 'cli') {

    $existing_values = array(
        'plugin_slug' => 'wordpressplugin',
        'plugin_name' => 'Wordpressplugin',
        'plugin_title' => 'WORDPRESSPLUGIN',
        'plugin_desc' => 'WORDPRESSPLUGINDESCRIPTION',
        'url' => 'http://example.com',
        'author_name' => 'Plugin Author',
        'author_email' => 'mail@example.com',
    );

    $exclude_directories = array('bin', 'base');

    mb_internal_encoding('UTF-8');

    $plugin_directory = dirname(dirname(__FILE__));

    echo $plugin_directory . PHP_EOL;

    $Directory = new RecursiveDirectoryIterator($plugin_directory);
    $objects = new RecursiveIteratorIterator($Directory);

    // Loop through all the files in all the directories
    foreach ($objects as $name => $object) {
        /* @var SplFileInfo $object */
        $path_name = $object->getPath();
        $relative_directory = preg_replace('/^\//', '', str_replace($plugin_directory, '', $path_name));

        // Check whether we need to exclude this path
        $exclude_directory = false;
        foreach ($exclude_directories as $directory) {
            if (preg_match('/^' . $directory . '/', $relative_directory) === 1) {
                $exclude_directory = true;
                break;
            }
        }

        if ($exclude_directory) {
            continue;
        }

        $existing_file_name = $object->getFilename();
        $new_file_name = str_replace($existing_values['plugin_slug'], $new_values['plugin_slug'], $existing_file_name);

        if ($object->isFile() && $object->isWritable()) {
            if ($object->isFile() && $object->isWritable() && preg_match('/^\./', $existing_file_name) === 0) {
                $file = $object->openFile('r');

                if ($file->getSize() > 0) {
                    $contents = $file->fread($file->getSize());

                    // String replaces on the contents of the file
                    $contents = str_replace($existing_values, $new_values, $contents);
                    $file = $object->openFile('w');
                    $file->fwrite($contents);
                }

                // Rename the file
                rename($path_name . '/' . $existing_file_name, $path_name . '/' . $new_file_name);

                echo 'Updating ' . preg_replace('/^\//', '', $relative_directory . '/' . $existing_file_name) . PHP_EOL;
            }
        }
    }
}