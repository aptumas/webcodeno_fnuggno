<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link                 http://webcode.no
 * @since                1.0.0
 * @package              Webcodeno_Fnuggno
 * @author               James Inglis <james@webcode.no>
 *
 * @wordpress-plugin
 * Plugin Name:          Webcode.no Fnugg.no
 * Plugin URI:           http://webcode.no
 * Description:          WordPress integration with Fnugg.no ski resort data with optional Statuspage.io integration
 * Version:              1.1.2
 * Author:               Webcode AS
 * Author URI:           http://webcode.no
 * License:              GPL-2.0+
 * License URI:          http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:          webcodeno_fnuggno
 * Bitbucket Plugin URI: https://bitbucket.org/webcodeas/webcodeno_fnuggno
 * Bitbucket Branch:     master
 * Domain Path:          /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-webcodeno_fnuggno-activator.php
 */
function activate_webcodeno_fnuggno() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-webcodeno_fnuggno-activator.php';
	Webcodeno_Fnuggno_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-webcodeno_fnuggno-deactivator.php
 */
function deactivate_webcodeno_fnuggno() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-webcodeno_fnuggno-deactivator.php';
	Webcodeno_Fnuggno_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_webcodeno_fnuggno' );
register_deactivation_hook( __FILE__, 'deactivate_webcodeno_fnuggno' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-webcodeno_fnuggno.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_webcodeno_fnuggno() {

	$plugin = new Webcodeno_Fnuggno();
	$plugin->run();

}
run_webcodeno_fnuggno();
