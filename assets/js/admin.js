(function ($) {
    'use strict';

    /**
     * All of the code for your admin-facing JavaScript source
     * should reside in this file.
     *
     * Note: It has been assumed you will write jQuery code here, so the
     * $ function reference has been prepared for usage within the scope
     * of this function.
     *
     * This enables you to define handlers, for when the DOM is ready:
     *
     * $(function() {
	 *
	 * });
     *
     * When the window is loaded:
     *
     * $( window ).load(function() {
	 *
	 * });
     *
     * ...and/or other possibilities.
     *
     * Ideally, it is not considered best practise to attach more than a
     * single DOM-ready or window-load handler for a particular page.
     * Although scripts in the WordPress core, Plugins and Themes may be
     * practising this, we should strive to set a better example in our own work.
     */
    $(function () {
        $('.hidden_field').closest('tr').hide();

        $('input#webcodeno_fnuggno_enable_statuspage').on('change', function (event) {
            if ($(this).is(':checked') === false) {
                $('select.slopes_status, select.lifts_status').val('fnugg');
            }
        });
    });

})(jQuery);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJ3ZWJjb2Rlbm9fZm51Z2duby1hZG1pbi5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gKCQpIHtcbiAgICAndXNlIHN0cmljdCc7XG5cbiAgICAvKipcbiAgICAgKiBBbGwgb2YgdGhlIGNvZGUgZm9yIHlvdXIgYWRtaW4tZmFjaW5nIEphdmFTY3JpcHQgc291cmNlXG4gICAgICogc2hvdWxkIHJlc2lkZSBpbiB0aGlzIGZpbGUuXG4gICAgICpcbiAgICAgKiBOb3RlOiBJdCBoYXMgYmVlbiBhc3N1bWVkIHlvdSB3aWxsIHdyaXRlIGpRdWVyeSBjb2RlIGhlcmUsIHNvIHRoZVxuICAgICAqICQgZnVuY3Rpb24gcmVmZXJlbmNlIGhhcyBiZWVuIHByZXBhcmVkIGZvciB1c2FnZSB3aXRoaW4gdGhlIHNjb3BlXG4gICAgICogb2YgdGhpcyBmdW5jdGlvbi5cbiAgICAgKlxuICAgICAqIFRoaXMgZW5hYmxlcyB5b3UgdG8gZGVmaW5lIGhhbmRsZXJzLCBmb3Igd2hlbiB0aGUgRE9NIGlzIHJlYWR5OlxuICAgICAqXG4gICAgICogJChmdW5jdGlvbigpIHtcblx0ICpcblx0ICogfSk7XG4gICAgICpcbiAgICAgKiBXaGVuIHRoZSB3aW5kb3cgaXMgbG9hZGVkOlxuICAgICAqXG4gICAgICogJCggd2luZG93ICkubG9hZChmdW5jdGlvbigpIHtcblx0ICpcblx0ICogfSk7XG4gICAgICpcbiAgICAgKiAuLi5hbmQvb3Igb3RoZXIgcG9zc2liaWxpdGllcy5cbiAgICAgKlxuICAgICAqIElkZWFsbHksIGl0IGlzIG5vdCBjb25zaWRlcmVkIGJlc3QgcHJhY3Rpc2UgdG8gYXR0YWNoIG1vcmUgdGhhbiBhXG4gICAgICogc2luZ2xlIERPTS1yZWFkeSBvciB3aW5kb3ctbG9hZCBoYW5kbGVyIGZvciBhIHBhcnRpY3VsYXIgcGFnZS5cbiAgICAgKiBBbHRob3VnaCBzY3JpcHRzIGluIHRoZSBXb3JkUHJlc3MgY29yZSwgUGx1Z2lucyBhbmQgVGhlbWVzIG1heSBiZVxuICAgICAqIHByYWN0aXNpbmcgdGhpcywgd2Ugc2hvdWxkIHN0cml2ZSB0byBzZXQgYSBiZXR0ZXIgZXhhbXBsZSBpbiBvdXIgb3duIHdvcmsuXG4gICAgICovXG4gICAgJChmdW5jdGlvbiAoKSB7XG4gICAgICAgICQoJy5oaWRkZW5fZmllbGQnKS5jbG9zZXN0KCd0cicpLmhpZGUoKTtcblxuICAgICAgICAkKCdpbnB1dCN3ZWJjb2Rlbm9fZm51Z2dub19lbmFibGVfc3RhdHVzcGFnZScpLm9uKCdjaGFuZ2UnLCBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgICAgIGlmICgkKHRoaXMpLmlzKCc6Y2hlY2tlZCcpID09PSBmYWxzZSkge1xuICAgICAgICAgICAgICAgICQoJ3NlbGVjdC5zbG9wZXNfc3RhdHVzLCBzZWxlY3QubGlmdHNfc3RhdHVzJykudmFsKCdmbnVnZycpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9KTtcblxufSkoalF1ZXJ5KTtcbiJdLCJmaWxlIjoiYWRtaW4uanMiLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
