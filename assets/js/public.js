(function ($) {
    'use strict';

    $(function () {
        var newDiv = $('<div class="webcodeno_fnuggno_popup_box" />').append($('.webcodeno_fnuggno.summary .slopes_lifts_details'));
        $('body').append(newDiv);

        var viewportWidth = $(window).width();
        var slopesLiftsDetailsElement = $('.webcodeno_fnuggno_popup_box .slopes_lifts_details');
        var slopesLiftsTriggerElement = $('.webcodeno_fnuggno.summary .details a');

        var adjustBoxPosition = function (anchor_element) {
            if ($(anchor_element).length == 0) {
                return;
            }
            
            var offset = $(anchor_element).offset();

            viewportWidth = $(window).width();
            var boxWidth = parseInt(slopesLiftsDetailsElement.find('.statuspage_conditions').width());
            offset.top += 30;

            if ((offset.left + boxWidth) > viewportWidth) {
                offset.left = 'inherit';
                offset.right = 0;
            } else {
                offset.right = undefined;
            }

            slopesLiftsDetailsElement.css(offset);
        };

        $('html').on('click', function (event) {
            if (!$(event.target).closest(slopesLiftsDetailsElement).length && !$(event.target).is(slopesLiftsDetailsElement)) {
                if (slopesLiftsDetailsElement.is(":visible")) {
                    slopesLiftsDetailsElement.hide()
                }
            }
        });

        $(slopesLiftsTriggerElement).on('click', function (event) {
            event.preventDefault();
            event.stopPropagation();
            adjustBoxPosition($(this));
            slopesLiftsDetailsElement.toggle();
        });

        $(window).on('resize', function (event) {
            adjustBoxPosition(slopesLiftsTriggerElement);
        });
    });

})(jQuery);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJ3ZWJjb2Rlbm9fZm51Z2duby1wdWJsaWMuanMiXSwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uICgkKSB7XG4gICAgJ3VzZSBzdHJpY3QnO1xuXG4gICAgJChmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBuZXdEaXYgPSAkKCc8ZGl2IGNsYXNzPVwid2ViY29kZW5vX2ZudWdnbm9fcG9wdXBfYm94XCIgLz4nKS5hcHBlbmQoJCgnLndlYmNvZGVub19mbnVnZ25vLnN1bW1hcnkgLnNsb3Blc19saWZ0c19kZXRhaWxzJykpO1xuICAgICAgICAkKCdib2R5JykuYXBwZW5kKG5ld0Rpdik7XG5cbiAgICAgICAgdmFyIHZpZXdwb3J0V2lkdGggPSAkKHdpbmRvdykud2lkdGgoKTtcbiAgICAgICAgdmFyIHNsb3Blc0xpZnRzRGV0YWlsc0VsZW1lbnQgPSAkKCcud2ViY29kZW5vX2ZudWdnbm9fcG9wdXBfYm94IC5zbG9wZXNfbGlmdHNfZGV0YWlscycpO1xuICAgICAgICB2YXIgc2xvcGVzTGlmdHNUcmlnZ2VyRWxlbWVudCA9ICQoJy53ZWJjb2Rlbm9fZm51Z2duby5zdW1tYXJ5IC5kZXRhaWxzIGEnKTtcblxuICAgICAgICB2YXIgYWRqdXN0Qm94UG9zaXRpb24gPSBmdW5jdGlvbiAoYW5jaG9yX2VsZW1lbnQpIHtcbiAgICAgICAgICAgIGlmICgkKGFuY2hvcl9lbGVtZW50KS5sZW5ndGggPT0gMCkge1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdmFyIG9mZnNldCA9ICQoYW5jaG9yX2VsZW1lbnQpLm9mZnNldCgpO1xuXG4gICAgICAgICAgICB2aWV3cG9ydFdpZHRoID0gJCh3aW5kb3cpLndpZHRoKCk7XG4gICAgICAgICAgICB2YXIgYm94V2lkdGggPSBwYXJzZUludChzbG9wZXNMaWZ0c0RldGFpbHNFbGVtZW50LmZpbmQoJy5zdGF0dXNwYWdlX2NvbmRpdGlvbnMnKS53aWR0aCgpKTtcbiAgICAgICAgICAgIG9mZnNldC50b3AgKz0gMzA7XG5cbiAgICAgICAgICAgIGlmICgob2Zmc2V0LmxlZnQgKyBib3hXaWR0aCkgPiB2aWV3cG9ydFdpZHRoKSB7XG4gICAgICAgICAgICAgICAgb2Zmc2V0LmxlZnQgPSAnaW5oZXJpdCc7XG4gICAgICAgICAgICAgICAgb2Zmc2V0LnJpZ2h0ID0gMDtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgb2Zmc2V0LnJpZ2h0ID0gdW5kZWZpbmVkO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBzbG9wZXNMaWZ0c0RldGFpbHNFbGVtZW50LmNzcyhvZmZzZXQpO1xuICAgICAgICB9O1xuXG4gICAgICAgICQoJ2h0bWwnKS5vbignY2xpY2snLCBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgICAgIGlmICghJChldmVudC50YXJnZXQpLmNsb3Nlc3Qoc2xvcGVzTGlmdHNEZXRhaWxzRWxlbWVudCkubGVuZ3RoICYmICEkKGV2ZW50LnRhcmdldCkuaXMoc2xvcGVzTGlmdHNEZXRhaWxzRWxlbWVudCkpIHtcbiAgICAgICAgICAgICAgICBpZiAoc2xvcGVzTGlmdHNEZXRhaWxzRWxlbWVudC5pcyhcIjp2aXNpYmxlXCIpKSB7XG4gICAgICAgICAgICAgICAgICAgIHNsb3Blc0xpZnRzRGV0YWlsc0VsZW1lbnQuaGlkZSgpXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgICAgICAkKHNsb3Blc0xpZnRzVHJpZ2dlckVsZW1lbnQpLm9uKCdjbGljaycsIGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICAgICAgYWRqdXN0Qm94UG9zaXRpb24oJCh0aGlzKSk7XG4gICAgICAgICAgICBzbG9wZXNMaWZ0c0RldGFpbHNFbGVtZW50LnRvZ2dsZSgpO1xuICAgICAgICB9KTtcblxuICAgICAgICAkKHdpbmRvdykub24oJ3Jlc2l6ZScsIGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgYWRqdXN0Qm94UG9zaXRpb24oc2xvcGVzTGlmdHNUcmlnZ2VyRWxlbWVudCk7XG4gICAgICAgIH0pO1xuICAgIH0pO1xuXG59KShqUXVlcnkpO1xuIl0sImZpbGUiOiJwdWJsaWMuanMiLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
