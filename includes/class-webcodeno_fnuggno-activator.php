<?php

/**
 * Fired during plugin activation
 *
 * @link       http://webcode.no
 * @since      1.0.0
 *
 * @package    Webcodeno_Fnuggno
 * @subpackage Webcodeno_Fnuggno/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Webcodeno_Fnuggno
 * @subpackage Webcodeno_Fnuggno/includes
 * @author     James Inglis <james@webcode.no>
 */
class Webcodeno_Fnuggno_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate()
	{
		wp_schedule_event(self::get_schedule(), 'daily', 'webcodeno_fnugg_schedule');
	}

	protected static function get_schedule()
	{
		$today = new DateTime();
		$today->setTime(10, 30);
		return $today->format("U");
	}
}
