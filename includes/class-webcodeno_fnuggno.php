<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://webcode.no
 * @since      1.0.0
 *
 * @package    Webcodeno_Fnuggno
 * @subpackage Webcodeno_Fnuggno/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Webcodeno_Fnuggno
 * @subpackage Webcodeno_Fnuggno/includes
 * @author     James Inglis <james@webcode.no>
 */
class Webcodeno_Fnuggno
{

    /**
     * The loader that's responsible for maintaining and registering all hooks that power
     * the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      Webcodeno_Fnuggno_Loader $loader Maintains and registers all hooks for the plugin.
     */
    protected $loader;

    /**
     * The unique identifier of this plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string $plugin_name The string used to uniquely identify this plugin.
     */
    protected $plugin_name;

    /**
     * The current version of the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string $version The current version of the plugin.
     */
    protected $version;

    /**
     * Define the core functionality of the plugin.
     *
     * Set the plugin name and the plugin version that can be used throughout the plugin.
     * Load the dependencies, define the locale, and set the hooks for the admin area and
     * the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function __construct()
    {
        $this->load_dependencies(); // needs to load so we can get the meta class

        $this->plugin_name = Webcodeno_Fnuggno_Meta::getInstance()->get_plugin_name();
        $this->version = Webcodeno_Fnuggno_Meta::getInstance()->get_version();

        $this->set_locale();
        $this->define_structure_hooks();
        $this->define_admin_hooks();
        $this->define_public_hooks();
    }

    /**
     * Load the required dependencies for this plugin.
     *
     * Include the following files that make up the plugin:
     *
     * - Webcodeno_Fnuggno_Loader. Orchestrates the hooks of the plugin.
     * - Webcodeno_fnuggno_i18n. Defines internationalization functionality.
     * - Webcodeno_Fnuggno_Admin. Defines all hooks for the admin area.
     * - Webcodeno_Fnuggno_Public. Defines all hooks for the public side of the site.
     *
     * Create an instance of the loader which will be used to register the hooks
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function load_dependencies()
    {
        /**
         * The base framework for all the other base classes
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'base/bootstrap.php';

        /**
         * All meta information for the plugin loaded in here!
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-webcodeno_fnuggno-meta.php';

        /**
         * The class providing various library functions
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-webcodeno_fnuggno-library.php';

        /**
         * The class responsible for orchestrating the actions and filters of the
         * core plugin.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-webcodeno_fnuggno-loader.php';

        /**
         * The class responsible for defining internationalization functionality
         * of the plugin.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-webcodeno_fnuggno-i18n.php';

        /**
         * The class responsible for defining all structural parts.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'structure/class-webcodeno_fnuggno-structure.php';

        /**
         * The class responsible for defining all actions that occur in the admin area.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'api_access/class-webcodeno_fnuggno-apiaccess.php';

        /**
         * The class responsible for defining all actions that occur in the admin area.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-webcodeno_fnuggno-admin.php';

        /**
         * The class responsible for defining all actions that occur in the public-facing
         * side of the site.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/class-webcodeno_fnuggno-public.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/class-webcodeno_fnuggno-widget.php';

        $this->loader = new Webcodeno_Fnuggno_Loader();

    }

    /**
     * Define the locale for this plugin for internationalization.
     *
     * Uses the Webcodeno_fnuggno_i18n class in order to set the domain and to register the hook
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function set_locale()
    {

        $plugin_i18n = new Webcodeno_fnuggno_i18n();

        $this->loader->add_action('plugins_loaded', $plugin_i18n, 'load_plugin_textdomain');

    }

    private function define_structure_hooks()
    {
        $plugin_structure = new Webcodeno_Fnuggno_Structure();

        // Runs the custom schema update if the database version number has changed
        $this->loader->add_action('plugins_loaded', $plugin_structure, 'db_version_check');

        // Load the shortcodes
        $this->loader->add_action('init', $plugin_structure, 'register_shortcodes');

        // Load the custom post types
        $this->loader->add_action('init', $plugin_structure, 'register_custom_post_types');

        // Load the admin columns
        $this->loader->add_action('init', $plugin_structure, 'register_admin_columns');

        // Load the custom taxonomies
        $this->loader->add_action('init', $plugin_structure, 'register_custom_taxonomies');

        // Load the custom field types
        $this->loader->add_action('init', $plugin_structure, 'register_custom_field_groups');

        // Load the widgets
        $this->loader->add_action('widgets_init', $plugin_structure, 'register_widgets');

        // Add in our custom cron schedules
        $this->loader->add_filter('cron_schedules', $plugin_structure, 'register_schedules');

        // Register our scheduled tasks
        if ($plugin_structure->scheduled_tasks_exist()) {
            foreach ($plugin_structure->get_scheduled_tasks() as $action => $scheduled_task) {
                $this->loader->add_action($action, $plugin_structure, $scheduled_task['callback']);
            }
        }
    }

    /**
     * Register all of the hooks related to the admin area functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function define_admin_hooks()
    {

        $plugin_admin = new Webcodeno_Fnuggno_Admin();

        $this->loader->add_filter('plugin_action_links_' . $this->plugin_name . '/' . $this->plugin_name . '.php',
            $plugin_admin, 'add_settings_link');
        $this->loader->add_action('admin_menu', $plugin_admin, 'add_options_page');
        $this->loader->add_action('admin_init', $plugin_admin, 'register_settings_base');

//		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts');

    }

    /**
     * Register all of the hooks related to the public-facing functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function define_public_hooks()
    {

        $plugin_public = new Webcodeno_Fnuggno_Public();

        $this->loader->add_action('webcodeno_fnugg_schedule', $plugin_public, 'flush_transients');

        $this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_styles');
        $this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_scripts');

    }

    /**
     * Run the loader to execute all of the hooks with WordPress.
     *
     * @since    1.0.0
     */
    public function run()
    {
        $this->loader->run();
    }

    /**
     * The name of the plugin used to uniquely identify it within the context of
     * WordPress and to define internationalization functionality.
     *
     * @since     1.0.0
     * @return    string    The name of the plugin.
     */
    public function get_plugin_name()
    {
        return $this->plugin_name;
    }

    /**
     * The reference to the class that orchestrates the hooks with the plugin.
     *
     * @since     1.0.0
     * @return    Webcodeno_Fnuggno_Loader    Orchestrates the hooks of the plugin.
     */
    public function get_loader()
    {
        return $this->loader;
    }

    /**
     * Retrieve the version number of the plugin.
     *
     * @since     1.0.0
     * @return    string    The version number of the plugin.
     */
    public function get_version()
    {
        return $this->version;
    }

}
