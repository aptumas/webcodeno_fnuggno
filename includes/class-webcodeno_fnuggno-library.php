<?php

/**
 * Created by PhpStorm.
 * User: james
 * Date: 30/12/15
 * Time: 13:51
 */
class Webcodeno_Fnuggno_Library
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $plugin_name The ID of this plugin.
     */
    protected $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $version The current version of this plugin.
     */
    protected $version;

    /**
     * Instance property to enable singleton operation
     *
     * @since    1.0.0
     * @access   protected
     * @var      Webcodeno_Fnuggno_Library $_instance Instance property to enable singleton operation
     */
    static protected $_instance;

    public function __construct()
    {
        $meta = Webcodeno_Fnuggno_Meta::getInstance();

        $this->plugin_name = $meta->get_plugin_name();
        $this->version = $meta->get_version();
    }

    /**
     * Retrieve the name of the highest priority template file that exists.
     *
     * Searches in the STYLESHEETPATH before TEMPLATEPATH so that themes which
     * inherit from a parent theme can just overload one file. If the template is
     * not found in either of those, it looks in the theme-compat folder last.
     *
     * Taken from bbPress
     *
     * @since v1.5
     *
     * @param string|array $template_names Template file(s) to search for, in order.
     * @param bool $load If true the template file will be loaded if it is found.
     * @param bool $require_once Whether to require_once or require. Default true.
     *                            Has no effect if $load is false.
     * @return string The template filename if one is located.
     */
    protected function locate_template($template_names, $load = false, $require_once = true)
    {
        // No file found yet
        $located = false;

        // Try to find a template file
        foreach ((array)$template_names as $template_name) {

            // Continue if template is empty
            if (empty($template_name))
                continue;

            // Trim off any slashes from the template name
            $template_name = ltrim($template_name, '/');

            // Check child theme first
            if (file_exists(trailingslashit(get_stylesheet_directory()) . $this->plugin_name . '/' . $template_name)) {
                $located = trailingslashit(get_stylesheet_directory()) . $this->plugin_name . '/' . $template_name;
                break;

                // Check parent theme next
            } elseif (file_exists(trailingslashit(get_template_directory()) . $this->plugin_name . '/' . $template_name)) {
                $located = trailingslashit(get_template_directory()) . $this->plugin_name . '/' . $template_name;
                break;

                // Check theme compatibility last
            } elseif (file_exists(trailingslashit(plugin_dir_path(__DIR__)) . 'templates/' . $template_name)) {
                $located = trailingslashit(plugin_dir_path(__DIR__)) . 'templates/' . $template_name;
                break;
            }
        }

        if ((true === $load) && !empty($located)) {
            load_template($located, $require_once);
        }

        return $located;
    }

    /**
     * Renders the template and catches the output in the output buffer for displaying later when appropriate
     *
     * @param string $template_name
     * @param string $global_variable_name
     * @param mixed $global_variable
     * @return string
     */
    public function render_template($template_name, $global_variable_name, $global_variable)
    {
        global ${$global_variable_name};
        ${$global_variable_name} = $global_variable;
        ob_start();
        $this->locate_template($template_name, true, false);
        $ret = ob_get_contents();
        ob_end_clean();
        return $ret;
    }

    public function parseMSJSONdate($date_string)
    {
        $match = preg_match('/\/Date\((\d+)([-+])(\d+)\)\//', $date_string, $date);

        $timestamp = $date[1] / 1000;
        $operator = $date[2];
        $hours = $date[3] * 36; // Get the seconds

        $datetime = new DateTime();

        $timezone_name = timezone_name_from_abbr('', $hours, 0);

        $datetime->setTimestamp($timestamp);
        $datetime->modify($operator . $hours . ' seconds');
        $datetime->setTimezone(new DateTimeZone($timezone_name));
        return $datetime->format('Y-m-d H:i:s O');
    }

    public function parseDate($date_string)
    {
        $match = preg_match('/([0-9\-T:.]+)([-+])(\d+):(\d+)/', $date_string, $date);

        if ($match === 0) {
            return null;
        }

        $timestamp = $date[1];
        $operator = $date[2];
        $hours = intval($date[3] . $date[4]) * 36; // Get the seconds

        $datetime = new DateTime($timestamp);

        $timezone_name = timezone_name_from_abbr('', $date[2] . $hours, 0);

        $datetime->modify($operator . $hours . ' seconds');
        $datetime->setTimezone(new DateTimeZone($timezone_name));
        return $datetime->format('Y-m-d H:i:s O');
    }

    /**
     * Takes a date string, parses it as DateTime object and outputs the format
     *
     * @param string $date_string
     * @param string $display_format
     * @return string
     */
    public function format_date($date_string, $display_format)
    {
        $datetime = new DateTime($date_string);
        if ($datetime) {
            $timezone_name = timezone_name_from_abbr('', $datetime->getOffset(), 0);
            $datetime->setTimezone(new DateTimeZone($timezone_name));
            $timestamp = $datetime->getTimestamp() + $datetime->getOffset();
            return date_i18n($display_format, $timestamp);
        }
        return '';
    }

    /**
     * Static singleton generator
     * @return mixed
     */
    static public function getInstance()
    {
        $class_name = get_called_class();
        if (!isset(self::$_instance)) {
            self::$_instance = new $class_name();
        }
        return self::$_instance;
    }
}