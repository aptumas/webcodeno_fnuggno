<?php

/**
 * The meta information for the plugin.
 *
 * Defines the plugin name, version and plugin title for use in other classes
 *
 * @link       http://webcode.no
 * @since      1.0.0
 * @package    Webcodeno_Fnuggno
 * @subpackage Webcodeno_Fnuggno/includes
 * @author     James Inglis <james@webcode.no>
 */
class Webcodeno_Fnuggno_Meta extends Webcodeno_Base_Meta_1_3_2
{
    /**
     * Webcodeno_Fnuggno_Meta constructor.
     */
    protected function __construct()
    {
        $this->config_file_location = dirname(dirname(__FILE__)) . '/config/';
        $this->init();
    }
}