var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cssnano = require('gulp-cssnano'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    cache = require('gulp-cache'),
    sequence = require('run-sequence'),
    sourcemaps = require('gulp-sourcemaps'),
    colors = require('colors'),
    gutil = require('gulp-util');

gulp.task('sass', function () {
    gulp.src('vendor/weather-icons-master/sass/weather-icons.scss')
        //.pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
        .pipe(gulp.dest('assets/css/'))
        .pipe(rename({suffix: '.min'}))
        .pipe(cssnano())
        //.pipe(sourcemaps.write())
        .pipe(gulp.dest('assets/css/'));
    gulp.src('public/scss/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
        .pipe(rename({basename: 'public'}))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('assets/css/'));
    gulp.src('public/scss/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
        .pipe(rename({basename: 'public', suffix: '.min'}))
        .pipe(cssnano())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('assets/css/'));
    gulp.src('admin/scss/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
        .pipe(rename({basename: 'admin'}))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('assets/css/'));
    gulp.src('admin/scss/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
        .pipe(rename({basename: 'admin', suffix: '.min'}))
        .pipe(cssnano())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('assets/css/'));
});

gulp.task('build-js', function () {
    gulp.src('public/js/*.js')
        .pipe(sourcemaps.init())
        .pipe(rename({basename: 'public'}))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('assets/js/'));
    gulp.src('public/js/*.js')
        .pipe(sourcemaps.init())
        .pipe(rename({basename: 'public', suffix: '.min'}))
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('assets/js/'));
    gulp.src('admin/js/*.js')
        .pipe(sourcemaps.init())
        .pipe(rename({basename: 'admin'}))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('assets/js/'));
    gulp.src('admin/js/*.js')
        .pipe(sourcemaps.init())
        .pipe(rename({basename: 'admin', suffix: '.min'}))
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('assets/js/'));
});

gulp.task('copy', function () {
    gulp.src('vendor/weather-icons-master/font/*')
        .pipe(gulp.dest('public/font/'))
});

// Build task
gulp.task('build', function (done) {
    sequence('copy', 'sass', done);
});

//Watch task
gulp.task('default', ['build'], function () {
    gulp.start('build-js');

    // Log file changes to console
    function logFileChange(event) {
        var fileName = require('path').relative(__dirname, event.path);
        console.log('[' + 'WATCH'.green + '] ' + fileName.magenta + ' was ' + event.type + ', running tasks...');
    }

    // Sass Watch
    gulp.watch(['admin/scss/*.scss', 'public/scss/*.scss'], ['sass'])
        .on('change', function (event) {
            logFileChange(event);
        });

    // JS Watch
    gulp.watch(['admin/js/*.js', 'public/js/*.js'], ['build-js'])
        .on('change', function (event) {
            logFileChange(event);
        });
});