<?php

/**
 * Created by PhpStorm.
 * User: james
 * Date: 30/12/15
 * Time: 11:04
 */
class Webcodeno_Fnuggno_Widget extends WP_Widget
{
    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string $plugin_name The ID of this plugin.
     */
    protected $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string $version The current version of this plugin.
     */
    protected $version;

    /**
     * Instance of Webcodeno_Fnuggno_Meta
     *
     * @since    1.0.1
     * @access   protected
     * @var Webcodeno_Fnuggno_Meta $meta
     */
    protected $meta;

    /**
     * Instance of Webcodeno_Fnuggno_APIAccess
     *
     * @since    1.0.1
     * @access   protected
     * @var Webcodeno_Fnuggno_APIAccess $api_access
     */
    protected $api_access;

    /**
     * Instance of Webcodeno_Fnuggno_Library
     *
     * @since    1.0.1
     * @access   protected
     * @var Webcodeno_Fnuggno_Library $library
     */
    protected $library;

    /**
     * Webcodeno_Fnuggno_Widget constructor.
     */
    public function __construct()
    {
        $this->meta = Webcodeno_Fnuggno_Meta::getInstance();

        $this->plugin_name = $this->meta->get_plugin_name();
        $this->version = $this->meta->get_version();

        $this->api_access = Webcodeno_Fnuggno_APIAccess::getInstance($this->plugin_name, $this->version);
        $this->library = Webcodeno_Fnuggno_Library::getInstance();

        $widget_ops = array(
            'class_name' => $this->meta->get_widget_name(),
            'description' => $this->meta->get_widget_description(),
        );

        parent::__construct($this->meta->get_widget_name(), $this->meta->get_widget_title(), $widget_ops);
    }

    /**
     * Outputs the content of the widget
     *
     * @param array $args
     * @param array $instance
     */
    public function widget($args, $instance)
    {

        echo $args['before_widget'];
        if (!empty($instance['title']) && !empty($instance['display_title']) && $instance['display_title'] === 'on') {
            echo $args['before_title'] . apply_filters('widget_title', $instance['title']) . $args['after_title'];
        }
//        var_dump($instance);
        if (intval($instance['resort_id']) !== 0) {
            $this->api_access->set_resort_id($instance['resort_id']);
            echo $this->library->render_template('widget.php', $this->meta->get_global_variable_name(), $this->api_access);
        } else {
            echo __('Resort ID has not been set.', $this->plugin_name);
        }

        echo $args['after_widget'];
    }

    /**
     * Outputs the options form on admin
     *
     * @param array $instance The widget options
     * @return boolean
     */
    public function form($instance)
    {
        $title = !empty($instance['title']) ? $instance['title'] : __('New title', 'text_domain');
        $display_title = !empty($instance['display_title']) && $instance['display_title'] === "on" ? true : false;
        $resort_id = !empty($instance['resort_id']) ? $instance['resort_id'] : '';
        $resort_select_list = $this->api_access->get_resort_select_list();

        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
                   name="<?php echo $this->get_field_name('title'); ?>" type="text"
                   value="<?php echo esc_attr($title); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('display_title'); ?>"><?php _e('Display Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('display_title'); ?>"
                   name="<?php echo $this->get_field_name('display_title'); ?>" type="checkbox"
                <?php checked($display_title); ?>>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('resort_id'); ?>"><?php _e('Resort:'); ?></label>
            <?php

            echo '<select class="widefat" name="' . $this->get_field_name('resort_id') . '" id="' . $this->get_field_id('resort_id') . '"> ';

            foreach ($resort_select_list as $option) {
                echo '<optgroup label="' . $option['label'] . '" />';
                if (array_key_exists('sub_values', $option) && is_array($option['sub_values'])) {
                    foreach ($option['sub_values'] as $subvalue) {
                        echo '<option value="' . $subvalue['value'] . '"' . selected($subvalue['value'], $resort_id, false) . '>' . $subvalue['label'] . '</option>';
                    }
                }

                echo '</optgroup>';
            }
            echo '</select>';

            ?>
        </p>
        <?php
        return true;
    }

    /**
     * Processing widget options on save
     *
     * @param array $new_instance The new options
     * @param array $old_instance The previous options
     * @return array
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        $instance['display_title'] = (!empty($new_instance['display_title'])) ? strip_tags($new_instance['display_title']) : '';
        $instance['resort_id'] = (!empty($new_instance['resort_id'])) ? strip_tags($new_instance['resort_id']) : '';

        return $instance;
    }
}