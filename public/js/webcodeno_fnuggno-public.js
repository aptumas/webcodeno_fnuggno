(function ($) {
    'use strict';

    $(function () {
        var newDiv = $('<div class="webcodeno_fnuggno_popup_box" />').append($('.webcodeno_fnuggno.summary .slopes_lifts_details'));
        $('body').append(newDiv);

        var viewportWidth = $(window).width();
        var slopesLiftsDetailsElement = $('.webcodeno_fnuggno_popup_box .slopes_lifts_details');
        var slopesLiftsTriggerElement = $('.webcodeno_fnuggno.summary .details a');

        var adjustBoxPosition = function (anchor_element) {
            if ($(anchor_element).length == 0) {
                return;
            }

            var offset = $(anchor_element).offset();

            viewportWidth = $(window).width();
            var boxWidth = parseInt(slopesLiftsDetailsElement.find('.statuspage_conditions').width());
            offset.top += 30;

            if ((offset.left + boxWidth) > viewportWidth) {
                offset.left = 'inherit';
                offset.right = 0;
            } else {
                offset.right = undefined;
            }

            slopesLiftsDetailsElement.css(offset);
        };

        $('html').on('click', function (event) {
            if (!$(event.target).closest(slopesLiftsDetailsElement).length && !$(event.target).is(slopesLiftsDetailsElement)) {
                if (slopesLiftsDetailsElement.is(":visible")) {
                    slopesLiftsDetailsElement.hide()
                }
            }
        });

        $(slopesLiftsTriggerElement).on('click', function (event) {
            event.preventDefault();
            event.stopPropagation();
            adjustBoxPosition($(this));
            slopesLiftsDetailsElement.toggle();
        });

        $(window).on('resize', function (event) {
            adjustBoxPosition(slopesLiftsTriggerElement);
        });
    });

})(jQuery);
