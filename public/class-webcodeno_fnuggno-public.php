<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://webcode.no
 * @since      1.0.0
 *
 * @package    Webcodeno_Fnuggno
 * @subpackage Webcodeno_Fnuggno/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Webcodeno_Fnuggno
 * @subpackage Webcodeno_Fnuggno/public
 * @author     James Inglis <james@webcode.no>
 */
class Webcodeno_Fnuggno_Public extends Webcodeno_Base_Public_1_3_2
{
    /**
     * @var Webcodeno_Fnuggno_APIAccess
     */
    public $api_access;

    /**
     * @var Webcodeno_fnuggno_Library
     */
    public $library;

    public function __construct()
    {
        $this->meta = Webcodeno_Fnuggno_Meta::getInstance();
        $this->api_access = Webcodeno_Fnuggno_APIAccess::getInstance();
        $this->library = Webcodeno_Fnuggno_Library::getInstance();
        $this->init();
    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {
        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/webcodeno_fnuggno-public.min.css', array(), $this->version, 'all');
        if ($this->get_setting('weather_icon_type')[0] === 'wi') {
            wp_enqueue_style($this->plugin_name . '-weather-icons', plugin_dir_url(__FILE__) . 'css/weather-icons.min.css', array(), $this->version, 'all');
        }
    }



    public function display_shortcode($attributes, $content)
    {
        $attributes = shortcode_atts(array(
            'resort_id' => null,
            'display' => 'all',
            'conditions' => 'slopes lifts',
            'popup_loaded' => false,
        ), $attributes);

        // This is a bit messy, but we needed to get the 'popup_loaded' back out of the global variable
        $global_variable_name = $this->meta->get_global_variable_name();
        global ${$global_variable_name};

        if (isset(${$global_variable_name}) && ${$global_variable_name}->get_shortcode_att('popup_loaded') === true) {
            $attributes['popup_loaded'] = true;
        }

        $attributes['conditions'] = explode(' ', $attributes['conditions']);

        $this->api_access->set_resort_id($attributes['resort_id']);
        $this->api_access->set_shortcode_atts($attributes);

        switch ($attributes['display']) {
            case 'summary':
                $template_name = 'shortcode_summary.php';
                break;
            case 'blog':
                $template_name = 'shortcode_blog.php';
                break;
            case 'conditions':
                $template_name = 'shortcode_conditions.php';
                break;
            case 'incidents':
                $template_name = 'shortcode_incidents.php';
                break;
            case 'attribution':
                $template_name = 'shortcode_attribution.php';
                break;
            default:
                $template_name = 'shortcode.php';
        }

        $this->enqueue_styles();
        $this->enqueue_scripts();

        return $this->library->render_template($template_name, $global_variable_name, $this->api_access);

    }

    public function display_data_shortcode($attributes, $content)
    {
        $attributes = shortcode_atts(array(
            'method' => '',
            'resort_id' => null,
        ), $attributes);

        if (!$this->api_access->is_shortcode_callable($attributes['method'])) {
            return '';
        }

        return $this->api_access->{$attributes['method']}($attributes, $content);
    }

}
