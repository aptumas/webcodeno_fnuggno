<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://webcode.no
 * @since      1.0.0
 *
 * @package    Webcodeno_Fnuggno
 * @subpackage Webcodeno_Fnuggno/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Webcodeno_Fnuggno
 * @subpackage Webcodeno_Fnuggno/admin
 * @author     James Inglis <james@webcode.no>
 */
class Webcodeno_Fnuggno_Admin extends Webcodeno_Base_Admin_1_3_2
{

    /**
     * @var Webcodeno_Fnuggno_APIAccess
     */
    public $api_access;

    public function __construct()
    {
        $this->meta = Webcodeno_Fnuggno_Meta::getInstance();
        $this->api_access = Webcodeno_Fnuggno_APIAccess::getInstance($this->plugin_name, $this->version);
        $this->init();
    }

    public function define_settings()
    {
        $resort_list_select_list = $this->api_access->get_resort_select_list();
        $resort_list = $this->api_access->get_resort_listing();
        $this->settings = array(
            'selected_resort' => array(
                'id' => 'selected_resort',
                'label' => __('Resort', $this->plugin_name),
                'type' => 'dropdown-optgroups',
                'attributes' => array(
                    'size' => 20,
                    'multiple' => 'multiple',
                    'style' => 'height: 180px;'
                ),
                'options' => $resort_list_select_list
            ),
            'enable_statuspage' => array(
                'id' => 'enable_statuspage',
                'label' => __('Enable Statuspage.io Data', $this->plugin_name),
                'type' => 'checkbox',
                'attributes' => array(
                    'size' => 20
                )
            ),
            'weather_icon_type' => array(
                'id' => 'weather_icon_type',
                'label' => __('Weather Icon Type', $this->plugin_name),
                'type' => 'dropdown',
                'attributes' => array(),
                'options' => array(
                    'png' => 'PNG',
                    'svg' => 'SVG',
                    'wi' => 'Weather Icons',
                )
            ),
            'enable_weather_name_mapping' => array(
                'id' => 'enable_weather_name_mapping',
                'label' => __('Enable Weather Name Mapping', $this->plugin_name),
                'type' => 'checkbox',
                'attributes' => array(
                    'size' => 20
                )
            ),
        );

        if ($this->get_setting('enable_weather_name_mapping', false)) {
            foreach ($this->get_yr_symbol_names() as $key => $name) {
                $settings_key = 'weather_name_mapping_' . $key;
                $this->settings[$settings_key] = array(
                    'id' => $settings_key,
                    'label' => sprintf(__('Weather Name: %s', $this->plugin_name), $name),
                    'type' => 'text',
                    'default_value' => $name,
                );
            }
        }

        foreach ($this->get_setting('selected_resort', array()) as $resort) {
            $key_slopes = 'slopes_status_' . $resort;
            $this->settings[$key_slopes] = array(
                'id' => $key_slopes,
                'label' => sprintf(__('Slopes Status for %s', $this->plugin_name), $resort_list[$resort]->name),
                'type' => 'dropdown',
                'attributes' => array(
                    'class' => 'hidden_field slopes_status',
                ),
                'options' => array(
                    'fnugg' => 'Fnugg.no',
                )
            );

            $key_lifts = 'lifts_status_' . $resort;
            $this->settings[$key_lifts] = array(
                'id' => $key_lifts,
                'label' => sprintf(__('Lift Status for %s', $this->plugin_name), $resort_list[$resort]->name),
                'type' => 'dropdown',
                'attributes' => array(
                    'class' => 'hidden_field lifts_status',
                ),
                'options' => array(
                    'fnugg' => 'Fnugg.no',
                )
            );

            if ($this->get_setting('enable_statuspage', '0') == 1) {
                $this->settings[$key_slopes]['options']['statuspage'] = 'Statuspage.io';
                $this->settings[$key_slopes]['attributes']['class'] = 'slopes_status';
                $this->settings[$key_lifts]['options']['statuspage'] = 'Statuspage.io';
                $this->settings[$key_lifts]['attributes']['class'] = 'lifts_status';

                $key_statuspage = 'statuspage_' . $resort;
                $this->settings[$key_statuspage] = array(
                    'id' => $key_statuspage,
                    'label' => sprintf(__('Statuspage.io API ID for %s', $this->plugin_name), $resort_list[$resort]->name),
                    'type' => 'text',
                    'attributes' => array(
                        'size' => 20
                    )
                );

                $key_groups_slopes = 'statuspage_groups_slopes_' . $resort;
                $this->settings[$key_groups_slopes] = array(
                    'id' => $key_groups_slopes,
                    'label' => sprintf(__('Statuspage.io Groups to Include as Slopes for %s', $this->plugin_name), $resort_list[$resort]->name),
                    'type' => 'checkboxes',
                    'attributes' => array(
                        'size' => 20
                    ),
                    'options' => $this->api_access->get_statuspage_group_options($resort),
                );

                $key_groups_lifts = 'statuspage_groups_lifts_' . $resort;
                $this->settings[$key_groups_lifts] = array(
                    'id' => $key_groups_lifts,
                    'label' => sprintf(__('Statuspage.io Groups to Include as Lifts for %s', $this->plugin_name), $resort_list[$resort]->name),
                    'type' => 'checkboxes',
                    'attributes' => array(
                        'size' => 20
                    ),
                    'options' => $this->api_access->get_statuspage_group_options($resort),
                );

                $key_label_operational = 'label_operational_' . $resort;
                $this->settings[$key_label_operational] = array(
                    'id' => $key_label_operational,
                    'label' => sprintf(__('Statuspage "Operational" Label for %s', $this->plugin_name), $resort_list[$resort]->name),
                    'type' => 'text',
                    'attributes' => array(
                        'size' => 20
                    ),
                    'default_value' => __('Operational', $this->plugin_name),
                );
                $key_label_non_operational = 'label_non_operational_' . $resort;
                $this->settings[$key_label_non_operational] = array(
                    'id' => $key_label_non_operational,
                    'label' => sprintf(__('Statuspage "Not Operational" Label for %s', $this->plugin_name), $resort_list[$resort]->name),
                    'type' => 'text',
                    'attributes' => array(
                        'size' => 20
                    ),
                    'default_value' => __('Not Operational', $this->plugin_name),
                    'description' => __('You can choose something a little more customer-friendly than "major outage"!', $this->plugin_name),
                );
            }
        }
    }

    public function get_resort_select_list()
    {
        $output = array();

        foreach ($this->api_access->get_region_listing() as $region_raw) {
            $region = $this->api_access->parse_region_property($region_raw);
            $region_id = $region->id;
            $output[$region_id] = array(
                'opt_group' => true,
                'label' => $region->title,
                'sub_values' => array()
            );
            foreach ($region->resorts as $resort_id) {
                $this_resort = $this->api_access->get_properties()['resort_listing'][$resort_id];
                $resort = array(
                    'value' => $resort_id,
                    'label' => sprintf('%s - %s (ID: %s)', $this_resort->name, $this_resort->contact->city, $resort_id),
                );
                $output[$region_id]['sub_values'][$resort_id] = $resort;
            }
            uasort($output[$region_id]['sub_values'], function ($a, $b) {
                return strcmp($a['label'], $b['label']);
            });
        }

        return $output;
    }

    public function get_yr_symbol_names()
    {
        return array(
            '1' => 'Sun',
            '2' => 'LightCloud',
            '3' => 'PartlyCloud',
            '4' => 'Cloud',
            '5' => 'LightRainSun',
            '6' => 'LightRainThunderSun',
            '7' => 'SleetSun',
            '8' => 'SnowSun',
            '9' => 'LightRain',
            '10' => 'Rain',
            '11' => 'RainThunder',
            '12' => 'Sleet',
            '13' => 'Snow',
            '14' => 'SnowThunder',
            '15' => 'Fog',
            '20' => 'SleetSunThunder',
            '21' => 'SnowSunThunder',
            '22' => 'LightRainThunder',
            '23' => 'SleetThunder',
            '24' => 'DrizzleThunderSun',
            '25' => 'RainThunderSun',
            '26' => 'LightSleetThunderSun',
            '27' => 'HeavySleetThunderSun',
            '28' => 'LightSnowThunderSun',
            '29' => 'HeavySnowThunderSun',
            '30' => 'DrizzleThunder',
            '31' => 'LightSleetThunder',
            '32' => 'HeavySleetThunder',
            '33' => 'LightSnowThunder',
            '34' => 'HeavySnowThunder',
            '40' => 'DrizzleSun',
            '41' => 'RainSun',
            '42' => 'LightSleetSun',
            '43' => 'HeavySleetSun',
            '44' => 'LightSnowSun',
            '45' => 'HeavysnowSun',
            '46' => 'Drizzle',
            '47' => 'LightSleet',
            '48' => 'HeavySleet',
            '49' => 'LightSnow',
            '50' => 'HeavySnow',
        );
    }

    public function get_weather_name_mapping_default()
    {
        $output = '';
        foreach ($this->get_yr_symbol_names() as $weather) {
            $output .= $weather . "," . $weather . PHP_EOL;
        }
        return $output;
    }
}
