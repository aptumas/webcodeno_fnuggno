<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://webcode.no
 * @since      1.0.0
 *
 * @package    Webcodeno_Fnuggno
 * @subpackage Webcodeno_Fnuggno/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="wrap">
    <h2><?php echo esc_html(get_admin_page_title()); ?></h2>

    <form action="options.php" method="post">
        <?php
        settings_fields($this->plugin_name);
        do_settings_sections($this->plugin_name);
        submit_button();
        ?>
    </form>

    <?php if ($this->meta->get_admin_enable_actions()): ?>
        <h2><?= __("Actions", $this->plugin_name); ?></h2>
        <p><?php $this->display_custom_action_buttons(); ?></p>
        <div class="action_output"><?= $this->action_output; ?></div>
    <?php endif; ?>

    <?php if (WP_DEBUG === true): ?>
        <h2><?= __("Debugging Information", $this->plugin_name); ?></h2>

    <?php endif; ?>
</div>