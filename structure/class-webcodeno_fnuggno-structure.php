<?php

/**
 * The definition of WordPress structures for the plugin.
 *
 * @version    1.1.2
 * @link       http://webcode.no
 * @since      1.0.0
 * @package    Wordpressplugin
 * @subpackage Wordpressplugin/api_access
 * @author     Plugin Author <mail@example.com>
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

if (!class_exists('Wordpressplugin_Structure')) {
    class Webcodeno_Fnuggno_Structure extends Webcodeno_Base_Structure_1_3_2
    {
        /**
         * @var Wordpressplugin_Library $library
         */
        public $library;
        /**
         * @var Wordpressplugin_Meta $meta
         */
        public $meta;

        public function __construct()
        {
            $this->meta = Webcodeno_Fnuggno_Meta::getInstance();
            $this->library = Webcodeno_Fnuggno_Library::getInstance();
            $this->init();
        }

        public function define_widgets()
        {
            $this->widgets = array(
                'Webcodeno_Fnuggno_Widget' => array(),
            );
        }

        public function define_shortcodes()
        {
            $this->shortcodes = array(
                'fnugg' => array(Webcodeno_Fnuggno_Public::getInstance(), 'display_shortcode'),
                'fnugg_data' => array(Webcodeno_Fnuggno_Public::getInstance(), 'display_data_shortcode'),
            );
        }

    }
}
