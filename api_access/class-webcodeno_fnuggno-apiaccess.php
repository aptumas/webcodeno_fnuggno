<?php

/**
 * The external API access-specific functionality of the plugin.
 *
 * Extends Webcodeno_fnuggno_APIAccessBase - more specific functionality to this plugin
 *
 * @link       http://webcode.no
 * @since      1.0.0
 * @package    Webcodeno_Fnuggno
 * @subpackage Webcodeno_Fnuggno/api_access
 * @author     James Inglis <james@webcode.no>
 */
class Webcodeno_Fnuggno_APIAccess extends Webcodeno_Base_APIAccess_1_3_2
{

    protected $api_type = "json";

    protected $api_object_type = "object";

    protected $api_args = array();

    /**
     * Storage array for shortcode attributes to be passed through to the template files
     *
     * @var array $shortcode_atts
     */
    protected $shortcode_atts = array();

    protected $region_listing_source = 'http://fnuggapi.cloudapp.net/search?index=blog&type=region';

    protected $resort_listing_source = 'http://fnuggapi.cloudapp.net/search?sourceFields=id,desc,name,contact,location&size=200';

    protected $resort_source = 'http://fnuggapi.cloudapp.net/get/resort/%%id%%';

    protected $resort_id;

    protected $statuspage_summary_source = 'http://%%statuspage_id%%.statuspage.io/api/v2/summary.json';

    protected $blog_source = 'http://fnuggapi.cloudapp.net/search?index=blog&sort=date:desc&facet=site:%%id%%';

    /**
     * @var Webcodeno_fnuggno_Library $library
     */
    public $library;

    /**
     * A list of properties in the api build query to add to the transient name
     * @var array $transient_keys_in_name
     */
    protected $transient_keys_in_name = array('id', 'statuspage_id');

    public function __construct()
    {
        $this->meta = Webcodeno_Fnuggno_Meta::getInstance();
        $this->library = Webcodeno_Fnuggno_Library::getInstance();
        $this->init();
    }

    /**
     * Stores shortcode attributes for retrieval in the template
     * @param array $shortcode_atts
     */
    public function set_shortcode_atts(array $shortcode_atts)
    {
        $this->shortcode_atts = $shortcode_atts;
    }

    /**
     * Updates a single value in the $shortcode_atts array
     * @param $key
     * @param $value
     */
    public function update_shortcode_att($key, $value)
    {
        $this->shortcode_atts[$key] = $value;
    }

    public function get_shortcode_atts()
    {
        return $this->shortcode_atts;
    }

    public function get_shortcode_att($key)
    {
        if (array_key_exists($key, $this->shortcode_atts)) {
            return $this->shortcode_atts[$key];
        }
        return null;
    }

    public function set_resort_id($resort_id = null)
    {
        if (!is_null($resort_id)) {
            $this->resort_id = $resort_id;
        } else {
            $this->resort_id = current($this->get_setting('selected_resort'));
        }
    }

    public function parse_region_listing_property($region_listing_raw)
    {
        // Do stuff to populate
        return $region_listing_raw->hits->hits;
    }

    public function get_region_listing()
    {
        return $this->get_api_property('region_listing');
    }

    public function parse_resort_listing_property($resort_listing_raw)
    {
        // Do stuff to populate
        $resorts = array();
        foreach ($resort_listing_raw->hits->hits as $resort) {
            $temp_resort = $this->parse_resort_property($resort);
            $id = $temp_resort->id;
            $resorts[$id] = $temp_resort;
        }
        return $resorts;
    }

    public function get_resort_listing()
    {
        return $this->get_api_property('resort_listing');
    }

    public function get_resort_select_list()
    {
        $this->get_region_listing();
        $this->get_resort_listing();

        $output = array();

        foreach ($this->get_region_listing() as $region_raw) {
            $region = $this->parse_region_property($region_raw);
            $region_id = $region->id;
            $output[$region_id] = array(
                'opt_group' => true,
                'label' => $region->title,
                'sub_values' => array()
            );
            foreach ($region->resorts as $resort_id) {
                $this_resort = $this->properties['resort_listing'][$resort_id];
                $resort = array(
                    'value' => $resort_id,
                    'label' => sprintf('%s - %s (ID: %s)', $this_resort->name, $this_resort->contact->city, $resort_id),
                );
                $output[$region_id]['sub_values'][$resort_id] = $resort;
            }
            uasort($output[$region_id]['sub_values'], function ($a, $b) {
                return strcmp($a['label'], $b['label']);
            });
        }

        return $output;
    }

    public function parse_region_property($region_raw)
    {
        return $region_raw->{'_source'};
    }

    public function parse_resort_property($resort_raw)
    {
        return $resort_raw->{'_source'};
    }

    public function parse_blog_property($blog_raw)
    {
        $blog_posts = array();
        foreach ($blog_raw->hits->hits as $blog_post) {
            $id = $blog_post->{'_id'};
            $blog_posts[$id] = $blog_post->{'_source'};
        }
        return $blog_posts;
    }

    public function parse_statuspage_summary_property($statuspage_summary_raw)
    {
        $up_status_array = array('operational');
        $statuspage_summary = array(
            'conditions' => array(),
            'incidents' => array(),
            'status' => array(),
        );

        if (is_object($statuspage_summary_raw) && property_exists($statuspage_summary_raw, 'components')) {
            // Process the component groups
            foreach ($statuspage_summary_raw->components as $component) {
                if ($component->group) {
                    $statuspage_summary['conditions'][$component->id] = array(
                        'id' => $component->id,
                        'name' => $component->name,
                        'status' => $component->status,
                        'position' => $component->position,
                        'operational' => in_array($component->status, $up_status_array),
                        'count' => 0,
                        'up' => 0,
                        'down' => 0
                    );
                }
            }

            // Process the components and add them as sub-array items to the appropriate group
            foreach ($statuspage_summary_raw->components as $component) {
                if (!$component->group) {
                    $statuspage_summary['conditions'][$component->group_id]['sub_values'][$component->id] = array(
                        'id' => $component->id,
                        'name' => $component->name,
                        'status' => $component->status,
                        'position' => $component->position,
                        'operational' => in_array($component->status, $up_status_array),
                    );

                    $statuspage_summary['conditions'][$component->group_id]['count']++;

                    if (in_array($component->status, $up_status_array)) {
                        $statuspage_summary['conditions'][$component->group_id]['up']++;
                    } else {
                        $statuspage_summary['conditions'][$component->group_id]['down']++;
                    }
                }
            }

            // Sort the groups
            uasort($statuspage_summary['conditions'], function ($a, $b) {
                if ($a['position'] == $b['position']) {
                    return 0;
                }
                return ($a['position'] < $b['position']) ? -1 : 1;
            });

            // Sort the items within the groups
            foreach ($statuspage_summary['conditions'] as &$group) {
                if (array_key_exists('sub_values', $group)) {
                    uasort($group['sub_values'], function ($a, $b) {
                        if ($a['position'] == $b['position']) {
                            return 0;
                        }
                        return ($a['position'] < $b['position']) ? -1 : 1;
                    });
                }
            }
            unset($group);

            // Process the incidents into an array
            foreach ($statuspage_summary_raw->incidents as $incident) {
                $statuspage_summary['incidents'][$incident->id] = array(
                    'id' => $incident->id,
                    'name' => $incident->name,
                    'status' => $incident->status,
                    'status_label' => ucwords($incident->status),
                    'created_at' => $this->library->parseDate($incident->created_at),
                    'updated_at' => $this->library->parseDate($incident->updated_at),
                    'monitoring_at' => $this->library->parseDate($incident->monitoring_at),
                    'resolved_at' => $this->library->parseDate($incident->resolved_at),
                    'impact' => $incident->impact,
                    'impact_label' => ucwords($incident->impact),
                    'updates' => array(),
                );
                foreach ($incident->incident_updates as $update) {
                    $statuspage_summary['incidents'][$incident->id]['updates'][$update->id] = array(
                        'id' => $update->id,
                        'status' => $update->status,
                        'status_label' => ucwords($update->status),
                        'body' => $update->body,
                        'created_at' => $this->library->parseDate($update->created_at),
                        'updated_at' => $this->library->parseDate($update->updated_at),
                        'display_at' => $this->library->parseDate($update->display_at),
                    );
                }
            }
        }

        return $statuspage_summary;
    }

    public function get_resort($resort_id = null)
    {
        if (!is_null($resort_id)) {
            $this->set_resort_id($resort_id);
        }
        $this->set_api_query_args(array(
            'id' => $this->resort_id,
        ));
        return $this->get_api_property('resort');
    }

    public function get_statuspage($resort_id = null)
    {
        $this->set_resort_id($resort_id);
        if (!$this->check_setting_exists('statuspage_' . $this->resort_id)) {
            return array();
        }
        $this->set_api_query_args(array(
            'statuspage_id' => $this->get_setting('statuspage_' . $this->resort_id),
        ));
        return $this->get_api_property('statuspage_summary');
    }

    public function get_weather_icon($raw = false)
    {
        $api_arguments = array(
            'id' => $this->resort_id,
        );
        $value = $this->get_value('resort', 'conditions::forecast::today::bottom::symbol::yr_id', $api_arguments);
        $weather_icon_type = is_array($this->get_setting('weather_icon_type')) ? $this->get_setting('weather_icon_type')[0] : 'default';

        switch ($weather_icon_type) {
            case 'wi':
                $link = sprintf('<i class="wi wi-yr-no-%s"></i>', $value);
                break;
            case 'svg':
                $icon_type = 'image/svg%2Bxml';
                $link = sprintf('<img src="http://api.yr.no/weatherapi/weathericon/1.1/?symbol=%s;content_type=%s" />', $value, $icon_type);
                break;
            default:
                $icon_type = 'image/png';
                $link = sprintf('<img src="http://api.yr.no/weatherapi/weathericon/1.1/?symbol=%s;content_type=%s" />', $value, $icon_type);
        }


        if ($raw) {
            return compact(array('icon-type', 'value', 'link'));
        }
        return $link;
    }

    /**
     * Gets the weather description, via text mapping function if appropriate
     *
     * @param bool $raw Unused
     * @return string
     */
    public function get_weather_desc($raw = false)
    {
        $api_arguments = array(
            'id' => $this->resort_id,
        );

        if ($this->get_setting('enable_weather_name_mapping', false)) {
            $yr_id = $this->get_value('resort', 'conditions::forecast::today::bottom::symbol::yr_id', $api_arguments);
            $mapped_value = $this->get_setting('weather_name_mapping_' . $yr_id, $this->get_value('resort', 'conditions::forecast::today::bottom::symbol::name', $api_arguments));
            return __($mapped_value, $this->plugin_name);
        } else {
            return __($this->get_value('resort', 'conditions::forecast::today::bottom::symbol::name', $api_arguments), $this->plugin_name);
        }
    }

    public function get_weather_temp($location, $raw = false)
    {
        $api_arguments = array(
            'id' => $this->resort_id,
        );
        $value = $this->get_value('resort', sprintf('conditions::current_report::%s::temperature::value', $location), $api_arguments);
        switch ($this->get_value('resort', sprintf('conditions::current_report::%s::temperature::unit', $location), $api_arguments)) {
            case 'farenheit':
                $unit = "F";
                break;
            default:
                $unit = "C";
        }
        if ($raw) {
            return compact(array('location', 'unit', 'value'));
        }
        return sprintf('<span class="temp">%s<span class="unit">&deg;%s</span></span>', $value, $unit);
    }

    public function get_weather_temp_peak($raw = false)
    {
        return $this->get_weather_temp('top', $raw);
    }

    public function get_weather_temp_base($raw = false)
    {
        return $this->get_weather_temp('bottom', $raw);
    }

    public function get_statuspage_group_options($resort = null)
    {
        $statuspage_summary = $this->get_statuspage($resort);
        $options = array();
        if (is_array($statuspage_summary) && array_key_exists('conditions', $statuspage_summary) && is_array($statuspage_summary['conditions'])) {
            foreach ($statuspage_summary['conditions'] as $group) {
                $options[$group['id']] = sprintf('%s (%s total, %s up)', $group['name'], $group['count'], $group['up']);
            }
        }
        return $options;
    }

    /**
     * @param bool $raw
     * @return array|string
     */
    public function get_slopes_summary($raw = false)
    {
        $slopes_up = 0;
        $slopes_all = 0;

        if ($this->get_setting('enable_statuspage', false) && $this->get_setting('slopes_status_' . $this->resort_id, array('fnugg'))[0] === 'statuspage') {
            $statuspage_summary = $this->get_statuspage($this->resort_id);
            $groups_to_include = $this->get_setting('statuspage_groups_slopes_' . $this->resort_id, array());


            foreach ($statuspage_summary['conditions'] as $group) {
                if (in_array($group['id'], $groups_to_include)) {
                    $slopes_up += $group['up'];
                    $slopes_all += $group['count'];
                }
            }
        } else {
            $api_arguments = array(
                'id' => $this->resort_id,
            );
            $slopes_up = $this->get_value('resort', 'slopes::open', $api_arguments);
            $slopes_all = $this->get_value('resort', 'slopes::count', $api_arguments);
        }

        if ($raw) {
            return compact(array('slopes_up', 'slopes_all'));
        }
        return sprintf('<span class="up">%s</span> / <span class="all">%s</span>', $slopes_up, $slopes_all);
    }

    public function get_wind($raw = false)
    {
        $api_arguments = array(
            'id' => $this->resort_id,
        );
        $mps = $this->get_value('resort', 'conditions::combined::top::wind::mps', $api_arguments);
        $name = $this->get_value('resort', 'conditions::combined::top::wind::name', $api_arguments);
        $degree = $this->get_value('resort', 'conditions::combined::top::wind::degree', $api_arguments);
        $speed = $this->get_value('resort', 'conditions::combined::top::wind::speed', $api_arguments);

        if ($raw) {
            return compact(array('mps', 'name', 'degree', 'speed'));
        }
        return sprintf('<span class="mps">%s<span class="unit">m/s</span></span> <span class="direction">%s<span class="unit">&deg;</span><span class="compass">%s</span></span> <span class="speed">%s</span>', $mps, $degree, $name, $speed);
    }

    public function get_snow($raw = false)
    {
        $api_arguments = array(
            'id' => $this->resort_id,
        );
        $units = $this->get_value('resort', 'conditions::combined::top::snow::unit', $api_arguments);
        $snow_today = $this->get_value('resort', 'conditions::combined::top::snow::today', $api_arguments);
        $depth_terrain = $this->get_value('resort', 'conditions::combined::top::snow::depth_terrain', $api_arguments);
        $depth_slope = $this->get_value('resort', 'conditions::combined::top::snow::depth_slope', $api_arguments);
        if ($raw) {
            return compact(array('units', 'snow_today', 'depth_terrain', 'depth_slope'));
        }
        return sprintf('<span class="terrain">%s<span class="unit"> %s</span></span> / <span class="slope">%s<span class="unit"> %s</span></span>', $depth_terrain, $units, $depth_slope, $units);
    }

    public function get_snow_terrain($raw = false)
    {
        $snow = $this->get_snow(true);

        $depth = $snow['depth_terrain'];
        $units = $snow['units'];

        if ($raw) {
            return compact(array('units', 'depth'));
        }
        return sprintf('<span class="terrain">%s<span class="unit"> %s</span>', $depth, $units);
    }

    public function get_snow_slope($raw = false)
    {
        $snow = $this->get_snow(true);

        $depth = $snow['depth_slope'];
        $units = $snow['units'];

        if ($raw) {
            return compact(array('units', 'depth'));
        }
        return sprintf('<span class="slope">%s<span class="unit"> %s</span>', $depth, $units);
    }

    public function get_lifts_summary($raw = false)
    {
        $lifts_open = 0;
        $lifts_total = 0;

        if ($this->get_setting('enable_statuspage', false) && $this->get_setting('lifts_status_' . $this->resort_id, array('fnugg'))[0] === 'statuspage') {
            $statuspage_summary = $this->get_statuspage($this->resort_id);
            $groups_to_include = $this->get_setting('statuspage_groups_lifts_' . $this->resort_id, array());


            foreach ($statuspage_summary['conditions'] as $group) {
                if (in_array($group['id'], $groups_to_include)) {
                    $lifts_open += $group['up'];
                    $lifts_total += $group['count'];
                }
            }
        } else {
            $api_arguments = array(
                'id' => $this->resort_id,
            );
            $lifts_open = $this->get_value('resort', 'lifts::open', $api_arguments);
            $lifts_total = $this->get_value('resort', 'lifts::count', $api_arguments);

        }

        if ($raw) {
            return compact(array('lifts_open', 'lifts_total'));
        }
        return sprintf('<span class="open">%s</span> / <span class="all">%s</span>', $lifts_open, $lifts_total);
    }

    public function get_conditions()
    {
        $api_arguments = array(
            'id' => $this->resort_id,
        );
        return $this->get_value('resort', 'conditions::current_report::bottom::condition_description', $api_arguments);
    }

    public function get_blog_posts()
    {
        $api_arguments = array(
            'id' => $this->resort_id,
        );
        return $this->get_value('blog', '', $api_arguments);
    }

    /**
     * @param array $types
     * @param bool $raw
     * @return string
     */
    public function get_statuspage_conditions(array $types, $raw = false)
    {
        $statuspage = $this->get_statuspage($this->resort_id);

        $show_groups = array();

        if (in_array('slopes', $types)) {
            $show_groups = array_merge($show_groups, $this->get_setting('statuspage_groups_slopes_' . $this->resort_id, array()));
        }

        if (in_array('lifts', $types)) {
            $show_groups = array_merge($show_groups, $this->get_setting('statuspage_groups_lifts_' . $this->resort_id, array()));
        }

        $group_data_output = array();

        foreach ($statuspage['conditions'] as $group) {
            if (in_array($group['id'], $show_groups)) {
                $group['operational_label'] = $group['operational'] ? $this->get_setting('label_operational_' . $this->resort_id) : $this->get_setting('label_non_operational_' . $this->resort_id);
                foreach ($group['sub_values'] as &$item) {
                    $item['operational_label'] = $item['operational'] ? $this->get_setting('label_operational_' . $this->resort_id) : $this->get_setting('label_non_operational_' . $this->resort_id);
                }
                unset($item);
                $group_data_output[] = $group;
            }
        }

        if ($raw) {
            return $group_data_output;
        }

        return $this->library->render_template('statuspage_conditions.php', 'statuspage_conditions', $group_data_output);
    }

    public function get_statuspage_incidents($raw = false)
    {
        $statuspage = $this->get_statuspage($this->resort_id);

        if ($raw) {
            return $statuspage['incidents'];
        }

        return $this->library->render_template('statuspage_incidents.php', 'statuspage_incidents', $statuspage['incidents']);
    }

}
