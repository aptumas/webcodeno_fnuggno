��    )      d  ;   �      �     �  
   �     �     �  &   �     �          +  	   <     F     Y     _  	   l     v     �     �     �     �     �     �     �     �  
   �  )   �  %        B  /   ^  0   �     �     �     �     �  Y   �  
   K     V     k       [   �  M   �     .  �  @     �	     �	     �	     
  "   )
     L
     h
     �
  	   �
     �
     �
     �
  	   �
     �
  	   �
     �
     �
     �
               $     ,  	   B  0   L  +   }     �  +   �  *   �          %     -     B  F   P  
   �     �     �     �  X   �  ?   )     i     &   '                           #   )      	                                         
   %              $   !                          (                                                                "    Base Blog Posts Debugging Information Display Title: Displays resort information from Fnugg Enable Statuspage.io Data Enable Weather Name Mapping General Settings Incidents Lift Status for %s Lifts More details New title Not Operational Operational Peak Resort Resort ID has not been set. Resort: Settings Slopes Slopes Status for %s Snow Depth Statuspage "Not Operational" Label for %s Statuspage "Operational" Label for %s Statuspage.io API ID for %s Statuspage.io Groups to Include as Lifts for %s Statuspage.io Groups to Include as Slopes for %s Summary Title: Weather Icon Type Weather Name: %s Weather and ski conditions data is sourced from %s, YR, Meteorological Institute and NRK. Webcode AS Webcode Fnugg Widget Webcode.no Fnugg.no Wind WordPress integration with Fnugg.no ski resort data with optional Statuspage.io integration You can choose something a little more customer-friendly than "major outage"! http://webcode.no Project-Id-Version: Webcode.no Fnugg.no
POT-Creation-Date: 2016-02-04 09:38+0100
PO-Revision-Date: 2016-02-04 23:10+0100
Last-Translator: 
Language-Team: 
Language: nb_NO
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.6
X-Poedit-Basepath: ..
X-Poedit-WPHeader: webcodeno_fnuggno.php
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Bunn Blogginnlegg Feilsøkingsinformasjon Tittel for tilpasset visning: Viser resort informasjon fra Fnugg Aktivere Statuspage.io Data Aktiver Vær tilordning Generelle innstillinger Hendelser Heisen Status for %s Heiser Les mer Ny tittel Ikke Operative Operative Topp Resort Resort-ID er ikke angitt. Resort: Instillinger Løyper Bakkene Status for %s Snødybde StatusPage “Ikke Operative” etiketten for %s StatusPage “Operative” etiketten for %s StatusPage.io API-ID %s StatusPage.io grupper med som heiser for %s StatusPage.io grupper ta som bakken for %s Oppsummering Tittel: YR.no Vær ikontypen Vær navn: %s Vær- og føredata er levert av %s, Yr, Meteorologisk institutt og NRK Webcode AS Webcode Fnugg Widget Webcode.no Fnugg.no Vind WordPress integrasjon med Fnugg.no ski resort data med valgfri Statuspage.io integrering Du kan velge noe litt mer kunde-vennlig enn “major outage”! http://webcode.no 