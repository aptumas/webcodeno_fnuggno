#, fuzzy
msgid ""
msgstr ""
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"Project-Id-Version: Webcode.no Fnugg.no\n"
"POT-Creation-Date: 2016-02-04 09:38+0100\n"
"PO-Revision-Date: 2015-12-29 21:23+0100\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.6\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-WPHeader: webcodeno_fnuggno.php\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;"
"_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"

#: admin/class-webcodeno_fnuggno-admin.php:45
msgid "Resort"
msgstr ""

#: admin/class-webcodeno_fnuggno-admin.php:56
msgid "Enable Statuspage.io Data"
msgstr ""

#: admin/class-webcodeno_fnuggno-admin.php:64
msgid "Weather Icon Type"
msgstr ""

#: admin/class-webcodeno_fnuggno-admin.php:75
msgid "Enable Weather Name Mapping"
msgstr ""

#: admin/class-webcodeno_fnuggno-admin.php:88
#, php-format
msgid "Weather Name: %s"
msgstr ""

#: admin/class-webcodeno_fnuggno-admin.php:99
#, php-format
msgid "Slopes Status for %s"
msgstr ""

#: admin/class-webcodeno_fnuggno-admin.php:112
#, php-format
msgid "Lift Status for %s"
msgstr ""

#: admin/class-webcodeno_fnuggno-admin.php:131
#, php-format
msgid "Statuspage.io API ID for %s"
msgstr ""

#: admin/class-webcodeno_fnuggno-admin.php:141
#, php-format
msgid "Statuspage.io Groups to Include as Slopes for %s"
msgstr ""

#: admin/class-webcodeno_fnuggno-admin.php:152
#, php-format
msgid "Statuspage.io Groups to Include as Lifts for %s"
msgstr ""

#: admin/class-webcodeno_fnuggno-admin.php:163
#, php-format
msgid "Statuspage \"Operational\" Label for %s"
msgstr ""

#: admin/class-webcodeno_fnuggno-admin.php:168
msgid "Operational"
msgstr ""

#: admin/class-webcodeno_fnuggno-admin.php:173
#, php-format
msgid "Statuspage \"Not Operational\" Label for %s"
msgstr ""

#: admin/class-webcodeno_fnuggno-admin.php:178
msgid "Not Operational"
msgstr ""

#: admin/class-webcodeno_fnuggno-admin.php:179
msgid ""
"You can choose something a little more customer-friendly than \"major outage"
"\"!"
msgstr ""

#: admin/partials/webcodeno_fnuggno-admin-display.php:28
msgid "Cache"
msgstr ""

#: admin/partials/webcodeno_fnuggno-admin-display.php:29
msgid "Flush Cached Data"
msgstr ""

#: admin/partials/webcodeno_fnuggno-admin-display.php:32
msgid "Debugging Information"
msgstr ""

#: base/class-webcodeno-admin-base.php:168
msgid "General Settings"
msgstr ""

#: base/class-webcodeno-meta.php:122
msgid "Webcode.no Plugin"
msgstr ""

#: base/class-webcodeno-meta.php:126
msgid "Webcode Plugin Widget"
msgstr ""

#: base/class-webcodeno-meta.php:127
msgid "Widget Description"
msgstr ""

#: base/class-webcodeno-meta.php:129
#: includes/class-webcodeno_fnuggno-meta.php:29
msgid "Settings"
msgstr ""

#. Plugin Name of the plugin/theme
#: includes/class-webcodeno_fnuggno-meta.php:22
msgid "Webcode.no Fnugg.no"
msgstr ""

#: includes/class-webcodeno_fnuggno-meta.php:26
msgid "Webcode Fnugg Widget"
msgstr ""

#: includes/class-webcodeno_fnuggno-meta.php:27
msgid "Displays resort information from Fnugg"
msgstr ""

#: public/class-webcodeno_fnuggno-widget.php:95
msgid "Resort ID has not been set."
msgstr ""

#: public/class-webcodeno_fnuggno-widget.php:109
msgid "New title"
msgstr ""

#: public/class-webcodeno_fnuggno-widget.php:116
msgid "Title:"
msgstr ""

#: public/class-webcodeno_fnuggno-widget.php:122
msgid "Display Title:"
msgstr ""

#: public/class-webcodeno_fnuggno-widget.php:128
msgid "Resort:"
msgstr ""

#: templates/shortcode.php:42 templates/shortcode_summary.php:40
msgid "Summary"
msgstr ""

#: templates/shortcode.php:51 templates/shortcode_summary.php:49
msgid "Peak"
msgstr ""

#: templates/shortcode.php:53 templates/shortcode_summary.php:51
msgid "Base"
msgstr ""

#: templates/shortcode.php:59 templates/shortcode_summary.php:57
msgid "Snow Depth"
msgstr ""

#: templates/shortcode.php:61 templates/shortcode_summary.php:59
msgid "Wind"
msgstr ""

#: templates/shortcode.php:66 templates/shortcode.php:85
#: templates/shortcode_conditions.php:37 templates/shortcode_summary.php:64
msgid "Slopes"
msgstr ""

#: templates/shortcode.php:67 templates/shortcode.php:88
#: templates/shortcode_conditions.php:38 templates/shortcode_summary.php:65
msgid "Lifts"
msgstr ""

#: templates/shortcode.php:68 templates/shortcode_summary.php:66
msgid "More details"
msgstr ""

#: templates/shortcode.php:91 templates/shortcode_incidents.php:41
msgid "Incidents"
msgstr ""

#: templates/shortcode.php:99 templates/shortcode_blog.php:40
msgid "Blog Posts"
msgstr ""

#: templates/shortcode.php:116 templates/shortcode_attribution.php:43
#, php-format
msgid ""
"Weather and ski conditions data is sourced from %s, YR, Meteorological "
"Institute and NRK."
msgstr ""

#. Plugin URI of the plugin/theme
#. Author URI of the plugin/theme
msgid "http://webcode.no"
msgstr ""

#. Description of the plugin/theme
msgid ""
"WordPress integration with Fnugg.no ski resort data with optional Statuspage."
"io integration"
msgstr ""

#. Author of the plugin/theme
msgid "Webcode AS"
msgstr ""
