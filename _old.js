var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cssnano = require('gulp-cssnano'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    cache = require('gulp-cache'),
    sequence = require('run-sequence'),
    sourcemaps = require('gulp-sourcemaps'),
    colors = require('colors'),
    gutil = require('gulp-util');

gulp.task('sass', function () {
    gulp.src('vendor/weather-icons-master/sass/weather-icons.scss')
        //.pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
        .pipe(gulp.dest('public/css/'))
        .pipe(rename({suffix: '.min'}))
        .pipe(cssnano())
        //.pipe(sourcemaps.write())
        .pipe(gulp.dest('public/css/'));
    gulp.src('public/scss/*.scss')
        //.pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
        .pipe(gulp.dest('public/css/'))
        .pipe(rename({suffix: '.min'}))
        .pipe(cssnano())
        //.pipe(sourcemaps.write())
        .pipe(gulp.dest('public/css/'));
});

gulp.task('copy', function () {
    gulp.src('vendor/weather-icons-master/font/*')
        .pipe(gulp.dest('public/font/'))
});

// Build task
gulp.task('build', function (done) {
    sequence('copy', 'sass', done);
});

// Default gulp task
// Run build task and watch for file changes
gulp.task('default', ['build'], function () {
    // Log file changes to console
    function logFileChange(event) {
        var fileName = require('path').relative(__dirname, event.path);
        console.log('[' + 'WATCH'.green + '] ' + fileName.magenta + ' was ' + event.type + ', running tasks...');
    }

    //gulp.start('sass', 'copy');

    // Sass Watch
    gulp.watch(['public/scss/*.scss'], ['sass'])
        .on('change', function (event) {
            logFileChange(event);
        });
});