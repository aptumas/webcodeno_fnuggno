<?php

/**
 * Template file for widget output of Webcode Fnugg plugin.
 *
 * Copy to /wp-content/themes/your-theme/webcodeno_fnuggno/widget.php to modify
 *
 * Methods available on the global $fnugg object:
 *
 * These methods all return a formatted string.
 * For arrays of raw data making up the string, add 'true' to the method (e.g. $fnugg->get_weather_icon(true) )
 * - $fnugg->get_weather_icon()
 * - $fnugg->get_weather_desc()
 * - $fnugg->get_weather_temp_peak()
 * - $fnugg->get_weather_temp_base()
 * - $fnugg->get_conditions()
 * - $fnugg->get_snow()
 * - $fnugg->get_wind()
 * - $fnugg->get_slopes_summary()
 * - $fnugg->get_lifts_summary()
 * - $fnugg->get_statuspage_summary() - uses the 'statuspage_summary.php' template
 *
 * The following methods return an array of objects to iterate through:
 * - $fnugg->get_blog_posts()
 *
 * Note: $fnugg->set_resort_id() has already been called before loading this template
 */

/**
 * @var Webcodeno_Fnuggno_APIAccess $fnugg
 */
global $fnugg;

?>

<div class="webcodeno_fnugg widget">
    <div>
        <div class="weather">
            <div class="icon"><?= $fnugg->get_weather_icon(); ?></div>
            <div class="description"><?= $fnugg->get_weather_desc(); ?></div>
            <div class="temp_peak"><?= $fnugg->get_weather_temp_peak(); ?></div>
            <div class="temp_base"><?= $fnugg->get_weather_temp_base(); ?></div>
        </div>
        <div class="conditions"><?= $fnugg->get_conditions(); ?></div>
        <div class="snow"><?= $fnugg->get_snow(); ?></div>
        <div class="wind"><?= $fnugg->get_wind(); ?></div>
        <div class="slopes"><?= $fnugg->get_slopes_summary(); ?></div>
        <div class="lifts"><?= $fnugg->get_lifts_summary(); ?></div>
    </div>
</div>