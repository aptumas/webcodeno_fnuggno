<?php

/**
 * Template file for shortcode output of Webcode Fnugg plugin.
 *
 * Copy to /wp-content/themes/your-theme/webcodeno_fnuggno/shortcode.php to modify
 *
 * Methods available on the global $fnugg object:
 *
 * These methods all return a formatted string.
 * For arrays of raw data making up the string, add 'true' to the method (e.g. $fnugg->get_weather_icon(true) )
 * - $fnugg->get_weather_icon()
 * - $fnugg->get_weather_desc()
 * - $fnugg->get_weather_temp_peak()
 * - $fnugg->get_weather_temp_base()
 * - $fnugg->get_conditions()
 * - $fnugg->get_snow()
 * - $fnugg->get_wind()
 * - $fnugg->get_slopes_summary()
 * - $fnugg->get_lifts_summary()
 * - $fnugg->get_statuspage_conditions(array('slopes','lifts')[, true]) - uses the 'statuspage_conditions.php' template
 *     - note the alternate argument structure
 * - $fnugg->get_statuspage_incidents() - uses the 'statuspage_incidents.php' template
 *
 * The following methods return an array of objects to iterate through:
 * - $fnugg->get_blog_posts()
 *
 * Note: $fnugg->set_resort_id() has already been called before loading this template
 */

/**
 * @var Webcodeno_Fnuggno_APIAccess $fnugg
 */
global $fnugg;

?>

<div class="webcodeno_fnuggno shortcode blog_posts">
    <div>
        <h3><?= __('Blog Posts', $fnugg->get_plugin_name()) ?></h3>
        <?php foreach ($fnugg->get_blog_posts() as $blog_post_id => $blog_post): ?>
            <article id="fnugg_blog_<?= $blog_post_id; ?>">
                <img src="<?= $blog_post->images->image_16_9_m ?>"/>
                <h4> <?= $blog_post->title; ?></h4>
                <p class="meta"><?= sprintf('Posted by %s %s on %s', $blog_post->author->first_name, $blog_post->author->last_name, date_i18n('l, F jS, Y', strtotime($blog_post->date))); ?></p>
                <p><?= nl2br($blog_post->description); ?></p>
            </article>
        <?php endforeach; ?>
    </div>
</div>