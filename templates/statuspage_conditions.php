<?php
/**
 * Template file for statuspage conditions output of Webcode Fnugg plugin.
 *
 * Copy to /wp-content/themes/your-theme/webcodeno_fnuggno/statuspage_conditions.php to modify
 */

/**
 * @var array $statuspage_conditions
 */
global $statuspage_conditions;
$library = Webcodeno_Fnuggno_Library::getInstance();
$api_access = Webcodeno_Fnuggno_APIAccess::getInstance();

?>

<div class="statuspage_conditions">
    <?php foreach ($statuspage_conditions as $group): ?>
        <div class="statuspage_group_wrapper group_<?= $group['id']; ?>">
            <div class="statuspage_group_title"><?= $group['name']; ?></div>
            <div
                class="statuspage_group_status <?= $group['operational'] ? 'operational' : 'not-operational'; ?>"><?= $group['operational_label']; ?>
                - <?= $group['up']; ?> / <?= $group['count']; ?></div>
            <div class="statuspage_items_wrapper">
                <?php foreach ($group['sub_values'] as $item): ?>
                    <div class="statuspage_item_wrapper item_<?= $item['id']; ?>">
                        <div class="statuspage_item_title"><?= $item['name'] ?></div>
                        <div
                            class="statuspage_item_status <?= $item['operational'] ? 'operational' : 'not-operational'; ?>"><?= $item['operational_label']; ?></div>
                        <div class="clearfix"></div>
                    </div>
                <?php endforeach; ?>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    <?php endforeach; ?>
</div>