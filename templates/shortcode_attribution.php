<?php

/**
 * Template file for shortcode output of Webcode Fnugg plugin.
 *
 * Copy to /wp-content/themes/your-theme/webcodeno_fnuggno/shortcode.php to modify
 *
 * Methods available on the global $fnugg object:
 *
 * These methods all return a formatted string.
 * For arrays of raw data making up the string, add 'true' to the method (e.g. $fnugg->get_weather_icon(true) )
 * - $fnugg->get_weather_icon()
 * - $fnugg->get_weather_desc()
 * - $fnugg->get_weather_temp_peak()
 * - $fnugg->get_weather_temp_base()
 * - $fnugg->get_conditions()
 * - $fnugg->get_snow()
 * - $fnugg->get_wind()
 * - $fnugg->get_slopes_summary()
 * - $fnugg->get_lifts_summary()
 * - $fnugg->get_statuspage_conditions(array('slopes','lifts')[, true]) - uses the 'statuspage_conditions.php' template
 *     - note the alternate argument structure
 * - $fnugg->get_statuspage_incidents() - uses the 'statuspage_incidents.php' template
 *
 * The following methods return an array of objects to iterate through:
 * - $fnugg->get_blog_posts()
 *
 * Note: $fnugg->set_resort_id() has already been called before loading this template
 */

/**
 * @var Webcodeno_Fnuggno_APIAccess $fnugg
 */
global $fnugg;

?>

<div class="webcodeno_fnuggno shortcode attribution">
    <div>
        <a href="http://fnugg.no"><img
                src="http://16u9cg49zqi3f13zf1fvdgzk.wpengine.netdna-cdn.com/wp-content/uploads/2015/10/Fnugg_logotype_horizontal_Pantone_306U.svg"
                alt="Fnugg"></a>
        <p><?= sprintf(__('Weather and ski conditions data is sourced from %s, YR, Meteorological Institute and NRK.', $fnugg->get_plugin_name()), '<a href="http://fnugg.no/">fnugg</a>'); ?></p>
    </div>
    <div class="clearfix"></div>
</div>