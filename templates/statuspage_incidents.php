<?php
/**
 * Template file for statuspage incidents output of Webcode Fnugg plugin.
 *
 * Copy to /wp-content/themes/your-theme/webcodeno_fnuggno/statuspage_summary.php to modify
 */

/**
 * @var array $statuspage_incidents
 */
global $statuspage_incidents;
$library = Webcodeno_Fnuggno_Library::getInstance();
$api_access = Webcodeno_Fnuggno_APIAccess::getInstance();

?>

<div class="statuspage_incidents">
    <?php foreach ($statuspage_incidents as $incident): ?>
        <div class="statuspage_incident_wrapper incident_<?= $incident['id']; ?>">

            <div class="statuspage_incident_title"><?= $incident['name']; ?></div>
            <div
                class="statuspage_incident_status <?= $incident['status']; ?>"><?= $incident['status_label']; ?></div>

            <div class="statuspage_incident_updates_wrapper">
                <?php foreach ($incident['updates'] as $update): ?>
                    <div class="statuspage_incident_update_wrapper incident_update_<?= $update['id']; ?>">
                        <div class="statuspage_incident_update_body"><?= $update['body'] ?></div>
                        <div
                            class="statuspage_incident_update_date"><?= $library->format_date($update['display_at'], "l jS F Y h:iA O") ?></div>
                        <div class="clearfix"></div>
                    </div>
                <?php endforeach; ?>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    <?php endforeach; ?>
</div>