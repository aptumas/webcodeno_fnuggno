<?php

/**
 * Template file for shortcode output of Webcode Fnugg plugin.
 *
 * Copy to /wp-content/themes/your-theme/webcodeno_fnuggno/shortcode.php to modify
 *
 * Methods available on the global $fnugg object:
 *
 * These methods all return a formatted string.
 * For arrays of raw data making up the string, add 'true' to the method (e.g. $fnugg->get_weather_icon(true) )
 * - $fnugg->get_weather_icon()
 * - $fnugg->get_weather_desc()
 * - $fnugg->get_weather_temp_peak()
 * - $fnugg->get_weather_temp_base()
 * - $fnugg->get_conditions()
 * - $fnugg->get_snow()
 * - $fnugg->get_wind()
 * - $fnugg->get_slopes_summary()
 * - $fnugg->get_lifts_summary()
 * - $fnugg->get_statuspage_conditions(array('slopes','lifts')[, true]) - uses the 'statuspage_conditions.php' template
 *     - note the alternate argument structure
 * - $fnugg->get_statuspage_incidents() - uses the 'statuspage_incidents.php' template
 *
 * The following methods return an array of objects to iterate through:
 * - $fnugg->get_blog_posts()
 *
 * Note: $fnugg->set_resort_id() has already been called before loading this template
 */

/**
 * @var Webcodeno_Fnuggno_APIAccess $fnugg
 */
global $fnugg;

$shortcode_attributes = $fnugg->get_shortcode_atts();

?>

<div class="webcodeno_fnuggno shortcode summary">
    <div>
        <h3><?= __('Summary', $fnugg->get_plugin_name()) ?></h3>
        <div class="container">
            <div class="col_container col1">
                <div class="weather">
                    <div class="icon"><?= $fnugg->get_weather_icon(); ?></div>
                    <div class="description"><?= $fnugg->get_weather_desc(); ?></div>
                    <div class="clearfix"></div>
                </div>
                <div class="temp">
                    <div class="temp_peak"><span class="label"><?= __('Peak', $fnugg->get_plugin_name()); ?>:</span> <?= $fnugg->get_weather_temp_peak(); ?>
                    </div>
                    <div class="temp_base"><span class="label"><?= __('Base', $fnugg->get_plugin_name()); ?>:</span> <?= $fnugg->get_weather_temp_base(); ?>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col_container col2">
                <div class="snow"><span class="label"><?= __('Snow Depth', $fnugg->get_plugin_name()); ?>
                        :</span> <?= $fnugg->get_snow_terrain(); ?></div>
                <div class="wind"><span class="label"><?= __('Wind', $fnugg->get_plugin_name()); ?>:</span> <?= $fnugg->get_wind(); ?></div>
                <div class="conditions_description"><?= $fnugg->get_conditions(); ?></div>
            </div>

            <div class="col_container col3">
                <div class="slopes"><span class="label"><?= __('Slopes', $fnugg->get_plugin_name()); ?>:</span> <?= $fnugg->get_slopes_summary(); ?></div>
                <div class="lifts"><span class="label"><?= __('Lifts', $fnugg->get_plugin_name()); ?>:</span> <?= $fnugg->get_lifts_summary(); ?></div>
                <div class="details"><a><?= __('More details', $fnugg->get_plugin_name()); ?></a></div>
                <?php if ($fnugg->get_shortcode_att('popup_loaded') === false): ?>
                    <div class="slopes_lifts_details popup">
                        <?= $fnugg->get_statuspage_conditions(array('lifts', 'slopes')); ?>
                    </div>
                    <?php $fnugg->update_shortcode_att('popup_loaded', true); ?>
                <?php endif; ?>
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
</div>

<?php if ($fnugg->get_setting('enable_statuspage', '0') == 1): ?>
    <div class="webcodeno_fnuggno shortcode statuspage">
        <div>
            <h3><?= __('Slopes', $fnugg->get_plugin_name()); ?></h3>
            <?= $fnugg->get_statuspage_conditions(array('slopes')); ?>

            <h3><?= __('Lifts', $fnugg->get_plugin_name()); ?></h3>
            <?= $fnugg->get_statuspage_conditions(array('lifts')); ?>

            <h3><?= __('Incidents', $fnugg->get_plugin_name()); ?></h3>
            <?= $fnugg->get_statuspage_incidents(); ?>
        </div>
    </div>
<?php endif; ?>

<div class="webcodeno_fnuggno shortcode blog_posts">
    <div>
        <h3><?= __('Blog Posts', $fnugg->get_plugin_name()) ?></h3>
        <?php foreach ($fnugg->get_blog_posts() as $blog_post_id => $blog_post): ?>
            <article id="fnugg_blog_<?= $blog_post_id; ?>">
                <img src="<?= $blog_post->images->image_16_9_m ?>"/>
                <h3> <?= $blog_post->title; ?></h3>
                <p class="meta"><?= sprintf('Posted by %s %s on %s', $blog_post->author->first_name, $blog_post->author->last_name, date_i18n('l, F jS, Y', strtotime($blog_post->date))); ?></p>
                <p><?= nl2br($blog_post->description); ?></p>
            </article>
        <?php endforeach; ?>
    </div>
</div>

<div class="webcodeno_fnuggno shortcode attribution">
    <div>
        <a href="http://fnugg.no"><img
                src="http://16u9cg49zqi3f13zf1fvdgzk.wpengine.netdna-cdn.com/wp-content/uploads/2015/10/Fnugg_logotype_horizontal_Pantone_306U.svg"
                alt="Fnugg"></a>
        <p><?= __('Weather and ski conditions data is sourced from %s, YR, Meteorological Institute and NRK.', $fnugg->get_plugin_name()); ?></p>
    </div>
    <div class="clearfix"></div>
</div>