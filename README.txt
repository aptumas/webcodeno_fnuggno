=== Webcode.no Fnugg.no ===
Contributors: James Inglis
Donate link: http://www.webcode.no
Tags: api, fnugg, statuspageio
Requires at least: 4.0
Tested up to: 4.4
Stable tag: 4.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

WordPress integration with Fnugg.no ski resort data with optional Statuspage.io integration

== Description ==

Provides access through WordPress to data retrieved from the Fnugg.no API, documented at http://fnuggapi.cloudapp.net/. Data can be displayed as a templateable page rendered through the [fnugg] shortcode, or you can access individual pieces of data/callbacks in the body of your content using the [fnugg_data] shortcode.

The following shortcodes are supported:

* [fnugg] (same as [fnugg display="all"])
* [fnugg display="summary"]
* [fnugg display="blog"]
* [fnugg display="conditions" conditions="slopes lifts"]
* [fnugg display="incidents"]
* [fnugg display="attribution"]
* [fnugg_data method="get_resort" resort_id="123"]

A templateable widget is also available.

More documentation to be written.

Please note: use of the Fnugg API requires attribution wherever the data is used, as per http://www.fnugg.no/artikler/api/. This attribution has been included on all default templates provided with this plugin. If you override these, it is your responsibility to ensure that you attribute this data in a compliant manner.

== Installation ==

1. Upload the `webcodeno_fnuggno` directory to the `/wp-content/plugins/` directory or upload the ZIP file through the WordPress Add Plugin interface
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Configure settings at wp-admin/options-general.php?page=webcodeno_fnuggno - select your resort and specific a Statuspage.io API ID if gathering additional data from that source.
4. Use the [fnugg] shortcode or activate the widget
5. Copy the the templates subdirectory to your theme directory to customize the template

== Frequently Asked Questions ==

TBA

== Screenshots ==

TBA

== Changelog ==

= 1.1.2 (2016-11-03) =
* Add safety check to Javascript

= 1.1.1 (2016-11-03) =
* Add safety checks for when options haven't been saved yet

= 1.1.0 (2016-02-26) =
* Moves plugin over to new base - lots of refactoring and restructuring! No difference to look or function! :)

= 1.0.12 (2016-02-04) =
* Updates translation files and translates all strings in templates

= 1.0.11 (2016-02-02) =
* Minor display updates

= 1.0.10 (2016-01-29) =
* Implements system to pass shortcode values to templates
* Updates all short code displays, including shortcode argument switches

= 1.0.9 (2016-01-28) =
* Add functions for data output in template
* Updates base template files
* Styling for new base templates
* Updates Gulp build process
* Implement popup window for slope/lift status
* Fixes issue in the base where singleton was not working correctly
* Minor layout changes

= 1.0.8 (2016-01-09) =
* Adds some methods to the base class and library class
* Update APIAccess and templates to get incidents from the Statuspage API
* Adds CSS for the new incidents page and amends to existing Sass class names

= 1.0.7 (2016-01-08) =
* Under the hood framework updates
* Plugin metadata updates

= 1.0.6 (2016-01-06) =
* Refactor all base classes to be self-contained
* Add cache clear functionality to invalidate data stored in transients

= 1.0.5 (2016-01-06) =
* Updates get_statuspage_summary() template method, giving it its own template to display data from the relevant Statuspage
* Updates translation files

= 1.0.4 (2016-01-05) =
* Adds admin fields for string overrides
* Implement Gulp compilation for Weather Icons compilation
* Implement further logic in the the get slopes and get lifts API methods
* Abstract out all common methods to a single base class
* Enables Weather Icons display

= 1.0.3 (2016-01-04) =
* Removes more debugging calls

= 1.0.2 (2016-01-04) =
* Display debugging information on admin page only when WP_DEBUG is true

= 1.0.1 (2016-01-04) =
* Update meta data with Bitbucket headers

= 1.0.0 (2015-12-30) =
* First version of the plugin

