<?php

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * Base abstract class - needs to be extended.
 *
 * @version    1.3.2
 * @link       http://webcode.no
 * @since      1.0.0
 * @package    Webcodeno_Base
 * @author     James Inglis <james@webcode.no>
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

if (!class_exists('Webcodeno_Base_Public_1_3_2')) {
    abstract class Webcodeno_Base_Public_1_3_2 extends Webcodeno_Base_1_3_2
    {
        public function init()
        {
            parent::init();
        }

        /**
         * Register the stylesheets for the public-facing side of the site.
         *
         * @since    1.0.0
         */
        public function enqueue_styles()
        {
            if ($this->meta->get_public_enqueue_styles()) {
                $filename_suffix = $this->is_global_debug_set() ? '' : '.min';
                wp_enqueue_style($this->get_plugin_name(), $this->get_plugin_uri_path() . 'assets/css/public' . $filename_suffix . '.css', array(), $this->get_version(), 'all');
            }

        }

        /**
         * Register the JavaScript for the public-facing side of the site.
         *
         * @since    1.0.0
         */
        public function enqueue_scripts()
        {
            if ($this->meta->get_public_enqueue_scripts()) {
                $filename_suffix = $this->is_global_debug_set() ? '' : '.min';
                wp_enqueue_script($this->get_plugin_name(), $this->get_plugin_uri_path() . 'assets/js/public' . $filename_suffix . '.js', array('jquery'), $this->get_version(), false);
            }
        }
    }
}
