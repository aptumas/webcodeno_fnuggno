<?php

/**
 * Trait to add job queueing to any other plugin class.
 *
 * @version    1.3.2
 * @link       http://webcode.no
 * @since      1.0.0
 * @package    Webcodeno_Base
 * @author     James Inglis <james@webcode.no>
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

if (!trait_exists('Webcodeno_Base_Queue_1_3_2')) {
    trait Webcodeno_Base_Queue_1_3_2
    {
        /**
         * Intended to be overridden by $queue_prefix
         * @var string $trait_queue_prefix
         */
        protected $trait_queue_prefix = '';
        /**
         * @var array $queue_items
         */
        protected $queue_items = array();

        /**
         * @var int $queue_length
         */
        protected $queue_length = 0;

        /**
         * @var int $queue_position
         */
        protected $queue_position = 0;

        /**
         * Intended to be overridden by $queue_tasks_per_cycle
         * @var int $trait_queue_tasks_per_cycle
         */
        protected $trait_queue_tasks_per_cycle = 10;

        /**
         * Intended to be overridden by $timeout_protection
         * @var int $trait_timeout_protection
         */
        protected $trait_timeout_protection = 30;

        protected function get_trait_property($property_name)
        {
            if (property_exists($this, $property_name)) {
                return $this->{$property_name};
            }
            return $this->{'trait_' . $property_name};
        }

        protected function get_queue_prefix()
        {
            $queue_prefix = $this->get_trait_property('queue_prefix');
            if (strlen($queue_prefix) > 0) {
                return $queue_prefix . '_';
            }
            return '';
        }

        protected function is_queue_running()
        {
            // This switch overrides any timeout protection
            if ($this->get_setting($this->get_queue_prefix() . 'queue_running', 0) == 0) {
                return false;
            }

            if (microtime(true) >= ($this->get_setting($this->get_queue_prefix() . 'queue_last_run', 0) + $this->get_trait_property('timeout_protection'))) {
                return true;
            }
            return false;
        }

        protected function setup_queue()
        {
            $hook_name = $this->get_plugin_name() . '_' . $this->get_queue_prefix() . 'queue';
            // Clear the temporary "queue" hook so ensure that only one event runs
            wp_clear_scheduled_hook($hook_name);
            wp_schedule_single_event(strtotime('+1 minute'), $hook_name);
        }

        protected function start_queue()
        {
            $this->update_setting($this->get_queue_prefix() . 'queue_running', 1);
            $this->update_setting($this->get_queue_prefix() . 'queue_last_run', microtime(true));
        }

        protected function finalize_queue()
        {
            // Only reset the position if we actually reached the end of the queue
            if ($this->queue_position === $this->queue_length) {
                wp_unschedule_event(wp_next_scheduled($this->get_plugin_name() . '_' . $this->get_queue_prefix() . 'queue'), $this->get_plugin_name() . '_' . $this->get_queue_prefix() . 'queue');
                $this->run_finalise_queue_task();
                $this->update_setting($this->get_queue_prefix() . 'queue_last_completed', date('U'));
                $this->update_setting($this->get_queue_prefix() . 'queue_position', 0);
            }
            $this->update_setting($this->get_queue_prefix() . 'queue_running', 0);
            $this->update_setting($this->get_queue_prefix() . 'queue_last_run', 0);
        }

        protected function run_queue()
        {
            $this->setup_queue();

            if ($this->is_queue_running() === false) {
                $this->start_queue();

                $this->run_pre_queue_task();
                if (is_array($this->queue_items) && count($this->queue_items) > 0) {
                    $this->queue_length = count($this->queue_items);
                    $this->queue_position = intval($this->get_setting($this->get_queue_prefix() . 'queue_position', 0));
                    $this_run_count = 0;
                    for ($i = $this->queue_position; $i < count($this->queue_items); $i++) {
                        $this->run_queue_task($this->queue_items[$i]);
                        $this->update_setting($this->get_queue_prefix() . 'queue_position', ++$this->queue_position);
                        $this_run_count++;

                        // If this is being run on page load, we want to limit the amount of cycles this does to prevent slowing down page load
                        if (php_sapi_name() !== "cli" && (!defined('DISABLE_WP_CRON') || DISABLE_WP_CRON !== true) && $this_run_count >= $this->get_trait_property('queue_tasks_per_cycle')) {
                            break;
                        }
                    }
                }
                $this->run_post_queue_task();

                $this->finalize_queue();
            }
        }

        /**
         * These are the methods that should be overriden in the class using the trait
         */

        /**
         * This method should set the $queue_items property
         */
        protected function run_pre_queue_task()
        {
        }

        protected function run_queue_task($queue_item)
        {
        }

        protected function run_post_queue_task()
        {
        }

        protected function run_finalise_queue_task()
        {

        }
    }
}