<?php

/**
 * The external API access-specific functionality of the plugin.
 *
 * Base abstract class - needs to be extended.
 *
 * @version    1.3.2
 * @link       http://webcode.no
 * @since      1.0.0
 * @package    Webcodeno_Base
 * @author     James Inglis <james@webcode.no>
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

if (!class_exists('Webcodeno_Base_Structure_1_3_2')) {
    abstract class Webcodeno_Base_Structure_1_3_2 extends Webcodeno_Base_1_3_2
    {
        /**
         * @var array $custom_post_types
         */
        protected $custom_post_types = array();

        /**
         * @var array $custom_taxonomies
         */
        protected $custom_taxonomies = array();

        /**
         * @var array $custom_field_groups
         */
        protected $custom_field_groups = array();

        /**
         * @var array $admin_columns
         */
        protected $admin_columns = array();

        /**
         * @var array $widgets
         */
        protected $widgets = array();

        /**
         * Array of shortcodes to register in the following format:
         *
         * array('shortcode' => array('Public_Class_Name', 'Method_Name')
         *
         * @var array $shortcodes
         */
        protected $shortcodes = array();

        /**
         * @var array $custom_schema
         */
        protected $custom_schema = array();

        /**
         * Array of tasks to schedule in the following format:
         *
         * array('action_name' => array(
         *   'timestamp' => time(),
         *   'recurrance' => 'daily',
         *   'args' => array(),
         *   'callback' => 'callback_method')
         * )
         *
         * @var array $scheduled_tasks
         */
        protected $scheduled_tasks = array();

        protected $schedules = array();

        public function init()
        {
            parent::init();
        }

        /**
         * Custom Post Types
         */

        /**
         * Sets the custom post type array - intended to be overridden!
         */
        protected function define_custom_post_types()
        {
            $this->custom_post_types = array();
        }

        /**
         * Runs the action of registering custom post types based on array
         */
        public function register_custom_post_types()
        {
            $this->define_custom_post_types();

            if (is_array($this->custom_post_types) && count($this->custom_post_types) > 0) {
                foreach ($this->custom_post_types as $slug => $custom_post_type) {
                    $custom_post_type['args']['labels'] = $custom_post_type['labels'];
                    register_post_type($slug, $custom_post_type['args']);
                }
            }
        }


        /**
         * Custom Taxonomies
         */

        /**
         * Sets the custom taxonomy array - intended to be overridden!
         */
        protected function define_custom_taxonomies()
        {
            $this->custom_taxonomies = array();
        }

        /**
         * Runs the action of registering custom taxonomy types based on array
         */
        public function register_custom_taxonomies()
        {
            $this->define_custom_taxonomies();

            if (is_array($this->custom_taxonomies) && count($this->custom_taxonomies) > 0) {
                foreach ($this->custom_taxonomies as $slug => $custom_taxonomy) {
                    //TODO Register the taxonomy
                }
            }
        }


        /**
         * Field Groups
         */

        /**
         * Sets the custom taxonomy array - intended to be overridden!
         */
        protected function define_custom_field_groups()
        {
            $this->custom_field_groups = array();
        }

        /**
         * Runs the action of registering custom field types based on array
         */
        public function register_custom_field_groups()
        {
            $this->define_custom_field_groups();

            if (is_array($this->custom_field_groups) && count($this->custom_field_groups) > 0) {
                foreach ($this->custom_field_groups as $slug => $custom_field_group) {
                    if (function_exists('acf_add_local_field_group')) {
                        acf_add_local_field_group($custom_field_group);
                    }
                }
            }
        }


        /**
         * Admin Columns - configuration for Admin Columns Pro
         */

        /**
         * Sets the custom taxonomy array - intended to be overridden!
         *
         * Note: the format exported from ACP is in the format `ac_register_columns( 'slug', array())`
         * For this configuration array, make the 'slug' argument the array key, and the array argument a nested array
         */
        protected function define_admin_columns()
        {
            $this->admin_columns = array();
        }

        /**
         * Runs the action of registering custom field types based on array
         */
        public function register_admin_columns()
        {
            $this->define_admin_columns();

            if (is_array($this->admin_columns) && count($this->admin_columns) > 0) {
                foreach ($this->admin_columns as $slug => $admin_columns_config) {
                    if (function_exists('ac_register_columns')) {
                        ac_register_columns($slug, $admin_columns_config);
                    }
                }
            }
        }


        /**
         * Custom Database Schema
         */

        /**
         * Sets the custom database schema array - intended to be overridden!
         */
        protected function define_custom_schema()
        {
            $this->custom_schema = array();
        }

        /**
         * Registers our custom schema
         */
        public function register_custom_schema()
        {
            ob_start();
            $this->define_custom_schema();

            if (is_array($this->custom_schema) && count($this->custom_schema) > 0) {
                global $wpdb;
                $charset_collate = $wpdb->get_charset_collate();
                require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

                foreach ($this->custom_schema as $table_name => $table_definition) {
                    $db_table = $wpdb->prefix . $table_name;
                    $sql = "CREATE TABLE $db_table ($table_definition) $charset_collate;";
                    dbDelta($sql);
                }
            }

            // dbDelta tends to throw PHP notices - using ob_clean is a hacky way of supressing the warning messages
            // about headers being sent too soon
            ob_clean();
            $this->update_setting('version_number', $this->get_version());
        }

        /**
         * Checks the plugin version stored in the database and if it doesn't match, update the schema
         *
         * This should be set to run on the 'plugins_loaded' action
         */
        public function db_version_check()
        {
            if ($this->get_setting('version_number') != $this->get_version()) {
                $this->register_custom_schema();
            }
        }


        /**
         * Scheduled Tasks
         */

        /**
         * Sets the scheduled tasks array - intended to be overridden!
         */
        protected function define_scheduled_tasks()
        {
            $this->scheduled_tasks = array();
        }

        /**
         * Checks to see if scheduled tasks have been registered
         *
         * @return bool
         */
        public function scheduled_tasks_exist()
        {
            $this->define_scheduled_tasks();

            return is_array($this->scheduled_tasks) && count($this->scheduled_tasks) > 0 ? true : false;
        }

        /**
         * Gets the array of scheduled tasks, rekeying to add plugin name to the action
         *
         * @return array
         */
        public function get_scheduled_tasks()
        {
            $return_array = array();
            foreach ($this->scheduled_tasks as $action => $scheduled_task) {
                $key = $this->meta->get_plugin_name() . '_' . $action;
                $return_array[$key] = $scheduled_task;
            }
            return $return_array;
        }

        /**
         * Registers the scheduled tasks - intended to be run in the Activator
         */
        public function register_scheduled_tasks()
        {
            $this->define_scheduled_tasks();

            if ($this->scheduled_tasks_exist()) {
                foreach ($this->get_scheduled_tasks() as $action => $scheduled_task) {
                    wp_schedule_event($scheduled_task['timestamp'], $scheduled_task['recurrance'], $action, $scheduled_task['args']);
                }
            }
        }

        /**
         * Unregisters the scheduled tasks
         * Intended to be run in the Deactivator
         */
        public function unregister_scheduled_tasks()
        {
            $this->define_scheduled_tasks();

            if ($this->scheduled_tasks_exist()) {
                foreach ($this->get_scheduled_tasks() as $action => $scheduled_task) {
                    wp_clear_scheduled_hook($action);
                }
            }
        }


        /**
         * Sets the widgets array - intended to be overridden!
         */
        public function define_widgets()
        {
            $this->widgets = array();
        }


        public function register_widgets()
        {
            $this->define_widgets();
            foreach ($this->widgets as $name => $function) {
                register_widget($name);
            }
        }


        /**
         * Shortcodes
         */

        /**
         * Sets the shortcodes array - intended to be overridden!
         */
        public function define_shortcodes()
        {
            $this->shortcodes = array();
        }

        /**
         * Run by add_action on 'init' in the loader
         */
        public function register_shortcodes()
        {
            $this->define_shortcodes();
            foreach ($this->shortcodes as $name => $function) {
                add_shortcode($name, $function);
            }
        }


        /**
         * Schedules
         */

        /**
         * Sets the custom schedules array - can be overridden, but can be left as-is
         */
        public function define_schedules()
        {
            $this->schedules = array(
                '1min' => array(
                    'interval' => 1 * 60,
                    'display' => __('Every minute'),
                ),
                '2min' => array(
                    'interval' => 2 * 60,
                    'display' => __('Every 2 minutes'),
                ),
                '5min' => array(
                    'interval' => 5 * 60,
                    'display' => __('Every 5 minutes'),
                ),
                '10min' => array(
                    'interval' => 10 * 60,
                    'display' => __('Every 10 minutes'),
                ),
                '15min' => array(
                    'interval' => 15 * 60,
                    'display' => __('Every 15 minutes'),
                ),
                '20min' => array(
                    'interval' => 10 * 60,
                    'display' => __('Every 20 minutes'),
                ),
                '30min' => array(
                    'interval' => 15 * 60,
                    'display' => __('Every 30 minutes'),
                ),
            );
        }

        /**
         * Adds the schedules
         * @return array
         */
        public function register_schedules()
        {
            $this->define_schedules();

            return $this->schedules;
        }
    }
}
