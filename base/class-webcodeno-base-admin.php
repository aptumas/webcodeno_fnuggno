<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * Base abstract class - needs to be extended.
 *
 * @version    1.3.2
 * @link       http://webcode.no
 * @since      1.0.0
 * @package    Webcodeno_Base
 * @author     James Inglis <james@webcode.no>
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

if (!class_exists('Webcodeno_Base_Admin_1_3_2')) {
    abstract class Webcodeno_Base_Admin_1_3_2 extends Webcodeno_Base_1_3_2
    {
        /**
         * Array of settings to config on admin page
         *
         * @since    1.0.1
         * @access   protected
         * @var array $settings
         */
        protected $settings;

        /**
         * Array of custom actions to run on admin page load
         *
         * @since    1.0.2
         * @access   protected
         * @var array $custom_actions
         */
        protected $custom_actions;

        /**
         * Array of custom actions to run on admin page load
         *
         * @since    1.0.2
         * @access   protected
         * @var array $default_custom_actions
         */
        protected $default_custom_actions;

        /**
         * The display output of any custom action
         * @var string $action_output
         */
        public $action_output;

        public function init()
        {
            parent::init();
        }

        /**
         * Register the stylesheets for the admin area.
         *
         * @since    1.0.0
         */
        public function enqueue_styles()
        {
            if ($this->meta->get_admin_enqueue_styles()) {
                $filename_suffix = $this->is_global_debug_set() ? '' : '.min';
                wp_enqueue_style($this->get_plugin_name(), $this->get_plugin_uri_path() . 'assets/css/admin' . $filename_suffix . '.css', array(), $this->get_version(), 'all');
            }
        }

        /**
         * Register the JavaScript for the admin area.
         *
         * @since    1.0.0
         */
        public function enqueue_scripts()
        {
            if ($this->meta->get_admin_enqueue_scripts()) {
                $filename_suffix = $this->is_global_debug_set() ? '' : '.min';
                wp_enqueue_script($this->get_plugin_name(), $this->get_plugin_uri_path() . 'assets/js/admin' . $filename_suffix . '.js', array('jquery'), $this->get_version(), false);
            }
        }

        /**
         * Add settings link on plugin page
         *
         * @param $links
         * @return mixed
         */
        public function add_settings_link($links)
        {
            $settings_link = sprintf('<a href="%s?page=%s">%s</a>', $this->get_menu_page_slug(), $this->get_plugin_name(),
                __($this->meta->get_plugin_settings_label(), $this->get_plugin_name()));
            array_unshift($links, $settings_link);
            return $links;
        }

        /**
         * Add an options page under the Settings submenu
         *
         * @since  1.0.0
         */
        public function add_options_page()
        {
            add_submenu_page(
                $this->get_menu_page_slug(),
                __($this->meta->get_plugin_settings_page_label(), $this->get_plugin_name()),
                __($this->meta->get_plugin_settings_menu_label(), $this->get_plugin_name()),
                $this->meta->get_plugin_settings_page_capabilities(),
                $this->get_plugin_name(),
                array($this, 'display_options_page')
            );
        }

        public function get_menu_page_slug()
        {
            // Using info found at http://codex.wordpress.org/Administration_Menus
            switch ($this->meta->get_plugin_settings_menu_location()) {
                case 'dashboard':
                    return 'index.php';
                    break;
                case 'appearance':
                    return 'themes.php';
                    break;
                case 'plugins':
                    return 'plugins.php';
                    break;
                case 'users':
                    return 'users.php';
                    break;
                case 'tools':
                    return 'tools.php';
                    break;
                default:
                    return 'options-general.php';
            }
        }

        /**
         * Render the options page for plugin
         *
         * @since  1.0.0
         */
        public function display_options_page()
        {
            $this->run_custom_actions();
            include_once $this->get_path_to_this() . 'partials/' . $this->get_plugin_name() . '-admin-display.php';
        }

        protected function define_default_custom_actions()
        {
            $this->default_custom_actions = array(
                'flush_cache' => array(
                    'label' => 'Flush Cache',
                    'callback' => array($this, 'flush_transients'),
                    'debug_only' => false
                ),
            );
        }

        protected function define_custom_actions()
        {
            $this->custom_actions = array();
        }

        protected function get_custom_actions()
        {
            $actions = array();
            if ($this->meta->get_admin_enable_default_actions()) {
                $this->define_default_custom_actions();
                $actions = array_merge($actions, $this->default_custom_actions);
            }
            $this->define_custom_actions();
            $actions = array_merge($actions, $this->custom_actions);

            $default_action_array = array(
                'label' => '',
                'callback' => '__return_true',
                'debug_only' => false,
            );

            $output_array = array();

            foreach ($actions as $action => $setting) {
                $output_array[$action] = array_merge($default_action_array, $setting);
            }

            return $output_array;
        }

        public function display_custom_action_buttons()
        {
            $this->define_custom_actions();

            foreach ($this->get_custom_actions() as $action => $settings) {
                echo sprintf('<a href="/wp-admin/%s?page=%s&action=%s" class="button button-secondary">%s</a> ', $this->get_menu_page_slug(), $this->get_plugin_name(), $action, $settings['label']);
            }
        }

        protected function run_custom_actions()
        {
            if ($this->meta->get_admin_enable_actions()) {
                $this->define_custom_actions();

                if (array_key_exists('action', $_GET)) {
                    foreach ($this->get_custom_actions() as $action => $settings) {
                        if (WP_DEBUG !== true && $settings['debug_only'] === true) {
                            continue;
                        }
                        if ($_GET['action'] === $action) {
                            if (is_array($settings['callback'])) {
                                $class = $settings['callback'][0];
                                $method = $settings['callback'][1];
                                $this->action_output = $class::getInstance()->$method();
                            } else {
                                $function = $settings['callback'];
                                $this->action_output = $function();
                            }
                            return;
                        }
                    }
                }
            }
        }

        /**
         * Defines $settings property - designed to be overridden
         */
        protected function define_settings()
        {
            $this->settings = array();
        }

        /**
         * Register all the settings
         */
        public function register_settings_base()
        {
            add_settings_section(
                $this->get_plugin_name() . '_general',
                __('General Settings', $this->get_plugin_name()),
                function ($args) {
//                echo '<p>' . __('Please change the settings accordingly.', $this->get_plugin_name()) . '</p>';
                },
                $this->get_plugin_name()
            );

            $this->define_settings();

            $this->register_all_settings();
        }

        /**
         * Home-grown function to simplify creating multiple settings fields using the Settings API behind the scenes
         *
         * @version  1.2
         */
        protected function register_all_settings()
        {
            foreach ($this->settings as $setting) {
                $setting['setting_name'] = $this->get_plugin_name() . '_' . $setting['id'];
                add_settings_field(
                    $setting['setting_name'],
                    __($setting['label'], $this->get_plugin_name()),
                    function ($args) {
                        $attributes = "";
                        if (array_key_exists('attributes', $args['setting']) && is_array($args['setting']['attributes'])) {
                            foreach ($args['setting']['attributes'] as $key => $value) {
                                $attributes .= ' ' . $key . '="' . $value . '"';
                            }
                        }
                        $default_value = array_key_exists('default_value',
                            $args['setting']) ? $args['setting']['default_value'] : '';

                        $setting_value = get_option($args['setting']['setting_name'], $default_value);

                        if (!in_array($args['setting']['type'], array('dropdown', 'dropdown-optgroups', 'checkboxes')) && is_array($setting_value)) {
                            $setting_value = implode(",", $setting_value);
                        }

                        $options = array();
                        if (array_key_exists('options_callback', $args['setting'])) {
                            $options = call_user_func(array($this, $args['setting']['options_callback']));
                        } elseif (array_key_exists('options', $args['setting'])) {
                            $options = $args['setting']['options'];
                        }

                        switch ($args['setting']['type']) {
                            case 'textarea':
                                echo '<textarea type="text" name="' . $args['setting']['setting_name'] . '" id="' . $args['setting']['setting_name'] . '"' . $attributes . '>' . $setting_value . '</textarea>';
                                break;
                            case 'dropdown':
                                echo '<select name="' . $args['setting']['setting_name'] . '[]" id="' . $args['setting']['setting_name'] . '"' . $attributes . '> ';

                                foreach ($options as $option_id => $option_label) {
                                    $selected = '';
                                    if (is_array($setting_value) && in_array($option_id, $setting_value)) {
                                        $selected = ' selected="selected"';
                                    } else if ($option_id === $setting_value) {
                                        $selected = ' selected="selected"';
                                    }
                                    echo '<option value="' . $option_id . '"' . $selected . ' />' . $option_label . '</option>';
                                }
                                echo '</select>';
                                break;
                            case 'dropdown-optgroups':
                                echo '<select name="' . $args['setting']['setting_name'] . '[]" id="' . $args['setting']['setting_name'] . '"' . $attributes . '> ';

                                foreach ($options as $option) {
                                    echo '<optgroup label="' . $option['label'] . '" />';
                                    if (array_key_exists('sub_values', $option) && is_array($option['sub_values'])) {
                                        foreach ($option['sub_values'] as $subvalue) {
                                            $selected = '';
                                            if (is_array($setting_value) && in_array($subvalue['value'], $setting_value)) {
                                                $selected = ' selected="selected"';
                                            } else if ($subvalue['value'] === $setting_value) {
                                                $selected = ' selected="selected"';
                                            }
                                            echo '<option value="' . $subvalue['value'] . '"' . $selected . ' />' . $subvalue['label'] . '</option>';
                                        }
                                    }

                                    echo '</optgroup>';
                                }
                                echo '</select>';
                                break;
                            case 'checkbox':
                                echo '<input type="hidden" name="' . $args['setting']['setting_name'] . '" id="' . $args['setting']['setting_name'] . '" value="0"' . $attributes . ' /> ';
                                echo '<input type="checkbox" name="' . $args['setting']['setting_name'] . '" id="' . $args['setting']['setting_name'] . '" value ="1"' . $attributes . checked("1", $setting_value, false) . ' /> ';
                                break;
                            case 'checkboxes':
                                foreach ($options as $option_id => $option_label) {
                                    $checked = '';
                                    if (is_array($setting_value) && in_array($option_id, $setting_value)) {
                                        $checked = ' checked="checked"';
                                    } else if ($option_id === $setting_value) {
                                        $checked = ' checked="checked"';
                                    }
                                    echo '<input type="checkbox" name="' . $args['setting']['setting_name'] . '[]" id="' . $args['setting']['setting_name'] . '" value ="' . $option_id . '"' . $attributes . $checked . ' /> ' . $option_label . '<br />';
                                }
                                break;
                            case 'hidden':
                                echo '<input class="hidden_field" type="hidden" name="' . $args['setting']['setting_name'] . '" id="' . $args['setting']['setting_name'] . '" value="' . $setting_value . '"' . $attributes . ' /> ';
                                break;
                            default:
                                echo '<input type="text" name="' . $args['setting']['setting_name'] . '" id="' . $args['setting']['setting_name'] . '" value="' . $setting_value . '"' . $attributes . ' /> ';

                        }

                        if (array_key_exists('description', $args['setting'])) {
                            echo "<p>" . $args['setting']['description'] . "</p>";
                        }

                    },
                    $this->get_plugin_name(),
                    $this->get_plugin_name() . '_general',
                    array('label_for' => $setting['setting_name'], 'setting' => $setting)
                );
                $sanitize_callback = array_key_exists('sanitize_callback', $setting) ? $setting['sanitize_callback'] : '';
                register_setting($this->get_plugin_name(), $setting['setting_name'], $sanitize_callback);
            }
        }
    }
}
