<?php

/**
 * The external API access-specific functionality of the plugin.
 *
 * Base abstract class - needs to be extended.
 *
 * @version    1.3.2
 * @link       http://webcode.no
 * @since      1.0.0
 * @package    Webcodeno_Base
 * @author     James Inglis <james@webcode.no>
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

if (!class_exists('Webcodeno_Base_APIAccess_1_3_2')) {
    abstract class Webcodeno_Base_APIAccess_1_3_2 extends Webcodeno_Base_1_3_2
    {
        /**
         * The menu label for the plugin settings admin page in the Settings menu.
         *
         * @since    1.0.0
         * @access   protected
         * @var      string $plugin_settings_page_label The menu label for the plugin settings admin page in the Settings menu.
         */
        protected $plugin_settings_label;

        /**
         * The page title for the plugin settings admin page.
         *
         * @since    1.0.0
         * @access   protected
         * @var      string $plugin_settings_page_label The page title for the plugin settings admin page.
         */
        protected $plugin_settings_page_label;

        protected $properties = array();

        protected $config_page = array();

        protected $api_type = "json";

        protected $api_object_type = "array";

        protected $api_args = array();

        protected $api_query_args = array();

        protected $shortcode_callable = array();

        /**
         * A list of properties in the api build query to add to the property/transient name
         *
         * @since    1.0.0
         * @access   protected
         * @var      array $property_keys_in_name A list of properties in the api build query to add to the property/transient name.
         */
        protected $property_keys_in_name = array('id');

        public function init()
        {
            parent::init();
            $this->transient_ttl = $this->meta->get_transient_ttl();
        }

        protected function build_api_query($api_query)
        {
            foreach ($this->api_query_args as $replacement => $value) {
                $api_query = str_replace('%%' . $replacement . '%%', urlencode($value), $api_query);
            }
            return $api_query;
        }

        /**
         * Main method to call API endpoint
         *
         * @param string $api_query
         * @param string $api_type
         * @param array $api_args
         * @return array|mixed|object|SimpleXMLElement|string|WP_Error
         */
        protected function call_api($api_query, $api_type = 'json', $api_args = array())
        {
            $response = wp_remote_get($this->build_api_query($api_query), $api_args);
            $response = wp_remote_retrieve_body($response);
            switch ($api_type) {
                case 'json':
                    return json_decode($response);
                    break;
                case 'xml':
                    return simplexml_load_string($response);
                    break;
                default:
                    return $response;
            }
        }

        /**
         * Add keys to transient name
         *
         * @param $property_name
         * @return string
         */
        protected function add_keys_to_name($property_name)
        {
            foreach ($this->property_keys_in_name as $key => $value) {
                if (in_array($value, array_keys($this->api_query_args))) {
                    $property_name .= '-' . $value . '_' . $this->api_query_args[$value];
                }
            }
            return $property_name;
        }

        protected function get_api_property($property_name, $raw = false)
        {
            $property_raw = $this->add_keys_to_name($property_name) . '_raw';
            $property_source = $property_name . '_source';

            if (!isset($this->properties[$property_raw])) {
                $transient = $this->get_transient_if_enabled($property_raw);
                if ($transient === false) {
                    $transient = $this->call_api($this->{$property_source}, $this->api_type, $this->api_args);
                    $this->set_transient_if_enabled($property_raw, $transient);
                }
                $this->properties[$property_raw] = $transient;

                $this->parse_raw_property($property_name);
            }

            if ($raw) {
                return $this->properties[$property_raw];
            }
            return $this->properties[$property_name];
        }

        protected function parse_raw_property($property_name)
        {
            $parse_method_name = 'parse_' . $property_name . '_property';
            $parse_method_name_keyed = 'parse_' . $this->add_keys_to_name($property_name) . '_property';
            $property_raw = $this->add_keys_to_name($property_name) . '_raw';

            if ($this->properties[$property_raw] !== null) {
                $transient = $this->get_transient_if_enabled($property_name);
                if ($transient === false || $this->transient_flush === true) {
                    if (method_exists($this, $parse_method_name)) {
                        $this->properties[$property_name] = $this->$parse_method_name($this->properties[$property_raw]);
                    } else if (method_exists($this, $parse_method_name_keyed)) {
                        $this->properties[$property_name] = $this->$parse_method_name_keyed($this->properties[$property_raw]);
                    } else {
                        $this->properties[$property_name] = $this->properties[$property_raw];
                    }
                    $this->set_transient_if_enabled($property_name, $this->properties[$property_name]);
                } else {
                    $this->properties[$property_name] = $transient;
                }
            } else {
                $this->properties[$property_name] = null;
            }

            return $this->properties[$property_name];
        }

        public function set_api_query_args($api_query_args = array(), $merge = false)
        {
            if ($merge) {
                $this->api_query_args = array_merge($this->api_query_args, $api_query_args);
            } else {
                $this->api_query_args = $api_query_args;
            }
        }

        /**
         * @param string $property_name
         * @param string $path
         * @param array $api_query_args
         * @return mixed
         */
        public function get_value($property_name, $path, $api_query_args = array())
        {
            $path_array = explode("::", $path);
            $this->set_api_query_args($api_query_args, true);
            $property = $this->get_api_property($property_name);

            for ($i = 0; $i < count($path_array); $i++) {
                if ($this->api_object_type === "array") {
                    if (is_array($property) && array_key_exists($path_array[$i], $property)) {
                        $property = $property[$path_array[$i]];
                    } else {
                        break;
                    }
                } else if ($this->api_object_type === "object") {
                    if (is_object($property) && property_exists($property, $path_array[$i])) {
                        $property = $property->{$path_array[$i]};
                    } else {
                        break;
                    }
                }
            }

            return $property;
        }

        public function is_shortcode_callable($method)
        {
            if ($method !== '' && method_exists($this, $method) && in_array($method, $this->shortcode_callable)) {
                return true;
            }
            return false;
        }

        public function get_properties()
        {
            return $this->properties;
        }

    }
}
