<?php

/**
 * The meta information for the plugin.
 *
 * Defines the plugin name, version and plugin title for use in other classes
 *
 * Base abstract class - needs to be extended.
 *
 * @version    1.3.2
 * @link       http://webcode.no
 * @since      1.0.0
 * @package    Webcodeno_Base
 * @author     James Inglis <james@webcode.no>
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

if (!class_exists('Webcodeno_Base_Meta_1_3_2')) {
    abstract class Webcodeno_Base_Meta_1_3_2
    {
        /**
         * Location of the config file
         *
         * @since    1.3.0
         * @access   protected
         * @var      string $config_file_location Location of the config file
         */
        protected $config_file_location;

        /**
         * Parsed config array from ini file
         *
         * @since    1.3.0
         * @access   protected
         * @var      array $config_array Parsed config array from ini file
         */
        protected $config_array;

        /**
         * The ID of this plugin.
         *
         * @since    1.0.1
         * @access   protected
         * @var      string $plugin_name The ID of this plugin.
         */
        protected $plugin_name;

        /**
         * The version of this plugin.
         *
         * @since    1.0.1
         * @access   protected
         * @var      string $version The current version of this plugin.
         */
        protected $version;

        /**
         * The human readable title of this plugin.
         *
         * @since    1.0.1
         * @access   protected
         * @var      string $plugin_title The human readable title of this plugin.
         */
        protected $plugin_title;

        /**
         * Instance property to enable singleton operation
         *
         * @since    1.0.1
         * @access   protected
         * @var      array $_instance Instance property array to enable singleton operation
         */
        static protected $_instance = array();

        /**
         * The ID of the widget.
         *
         * @since    1.0.1
         * @access   protected
         * @var      string $widget_name The ID of the widget
         */
        protected $widget_name;

        /**
         * The title of the widget.
         *
         * @since    1.0.1
         * @access   protected
         * @var      string $widget_title The title of the widget
         */
        protected $widget_title;

        /**
         * The description of the widget.
         *
         * @since    1.0.1
         * @access   protected
         * @var      string $widget_description The description of the widget
         */
        protected $widget_description;

        /**
         * Name of the global variable passed to templates
         *
         * @since    1.0.1
         * @access   protected
         * @var      string $global_variable_name Name of the global variable passed to templates
         */
        protected $global_variable_name;

        /**
         * The menu label for the plugin settings admin page in the Settings menu.
         *
         * @since    1.0.1
         * @access   protected
         * @var      string $plugin_settings_page_label The menu label for the plugin settings admin page in the Settings menu.
         */
        protected $plugin_settings_label;

        /**
         * The page title for the plugin settings admin page.
         *
         * @since    1.0.1
         * @access   protected
         * @var      string $plugin_settings_page_label The page title for the plugin settings admin page.
         */
        protected $plugin_settings_page_label;

        /**
         * The capabilities required to access the plugin settings admin page.
         *
         * @since    1.3.0
         * @access   protected
         * @var      string $plugin_settings_page_capabilities The capabilities required to access the plugin settings admin page.
         */
        protected $plugin_settings_page_capabilities;

        /**
         * The menu label for the plugin settings admin page.
         *
         * @since    1.3.0
         * @access   protected
         * @var      string $plugin_settings_menu_label The menu label for the plugin settings admin page.
         */
        protected $plugin_settings_menu_label;

        /**
         * The location of the plugin settings admin page.
         *
         * @since    1.3.0
         * @access   protected
         * @var      string $plugin_settings_menu_location The location of the plugin settings admin page.
         */
        protected $plugin_settings_menu_location;

        /**
         * Sets whether to enable styles for the admin page
         *
         * @since    1.1.0
         * @access   protected
         * @var      bool $admin_enqueue_styles Sets whether to enable styles for the admin page
         */
        protected $admin_enqueue_styles;

        /**
         * Sets whether to enable scripts for the admin page
         *
         * @since    1.1.0
         * @access   protected
         * @var      bool $admin_enqueue_styles Sets whether to enable scripts for the admin page
         */
        protected $admin_enqueue_scripts;

        /**
         * Sets whether to enable any actions for the admin page
         *
         * @since    1.2.0
         * @access   protected
         * @var      bool $admin_enable_actions Sets whether to enable any actions for the admin page
         */
        protected $admin_enable_actions;

        /**
         * Sets whether to enable default actions for the admin page
         *
         * @since    1.2.0
         * @access   protected
         * @var      bool $admin_enable_default_actions Sets whether to enable default actions for the admin page
         */
        protected $admin_enable_default_actions;

        /**
         * Sets whether to enable styles for the public display
         *
         * @since    1.1.0
         * @access   protected
         * @var      bool $admin_enqueue_styles Sets whether to enable styles for the public display
         */
        protected $public_enqueue_styles;

        /**
         * Sets whether to enable scripts for the public display
         *
         * @since    1.1.0
         * @access   protected
         * @var      bool $admin_enqueue_styles Sets whether to enable scripts for the public display
         */
        protected $public_enqueue_scripts;

        /**
         * The time to live (TTL) for data cached in transients
         *
         * @since    1.0.1
         * @access   protected
         * @var      int $transient_ttl The time to live (TTL) for data cached in transients
         */
        protected $transient_ttl;

        /**
         * Loads config file from location defined in child class constructor
         *
         * @since    1.3.0
         * @throws   Exception
         */
        protected function load_config_file()
        {
            if (!isset($this->config_array)) {
                $this->config_array = parse_ini_file($this->config_file_location . 'config.ini', true);
            }
            if ($this->config_array === false) {
                throw new Exception('Unable to load config file.');
            }
        }

        /**
         * Webcodeno_Meta setup function.
         *
         * Populates base details but should definitely be overriden or added to!
         */
        protected function init()
        {
            $this->load_config_file();

            $this->plugin_name = $this->config_array['meta']['name'];
            $this->plugin_title = __($this->config_array['meta']['title'], $this->plugin_name);
            $this->version = $this->config_array['meta']['version'];

            $this->widget_name = $this->config_array['widget']['name'];
            $this->widget_title = __($this->config_array['widget']['title'], $this->plugin_name);
            $this->widget_description = __($this->config_array['widget']['description'], $this->plugin_name);

            $this->plugin_settings_label = __($this->config_array['settings_page']['plugin_page_label'], $this->plugin_name);
            $this->plugin_settings_page_label = __($this->config_array['settings_page']['page_title'], $this->plugin_name);
            $this->plugin_settings_page_capabilities = $this->config_array['settings_page']['page_capabilities'];
            $this->plugin_settings_menu_label = __($this->config_array['settings_page']['menu_label'], $this->plugin_name);
            $this->plugin_settings_menu_location = $this->config_array['settings_page']['menu_location'];
            $this->transient_ttl = get_option($this->plugin_name . '_transient_ttl', $this->config_array['misc']['transient_ttl'] * HOUR_IN_SECONDS);

            $this->admin_enable_actions = $this->config_array['settings_page']['enable_actions'] === "1" ? true : false;
            $this->admin_enable_default_actions = $this->config_array['settings_page']['enable_default_actions'] === "1" ? true : false;
            $this->admin_enqueue_styles = $this->config_array['settings_page']['enqueue_styles'] === "1" ? true : false;
            $this->admin_enqueue_scripts = $this->config_array['settings_page']['enqueue_scripts'] === "1" ? true : false;

            $this->public_enqueue_styles = $this->config_array['public_display']['enqueue_styles'] === "1" ? true : false;
            $this->public_enqueue_scripts = $this->config_array['public_display']['enqueue_scripts'] === "1" ? true : false;

            $this->global_variable_name = $this->config_array['misc']['global_variable_name'];
        }

        /**
         * Gets the ID of the plugin
         *
         * @since    1.0.1
         * @access   public
         * @return  string
         */
        public function get_plugin_name()
        {
            return $this->plugin_name;
        }

        /**
         * Gets the version of the plugin
         *
         * @since    1.0.1
         * @access   public
         * @return  string
         */
        public function get_version()
        {
            return $this->version;
        }

        /**
         * Gets the title of the plugin
         *
         * @since    1.0.1
         * @access   public
         * @return  string
         */
        public function get_plugin_title()
        {
            return $this->plugin_title;
        }

        /**
         * Gets the name of the widget
         *
         * @since    1.0.1
         * @access   public
         * @return  string
         */
        public function get_widget_name()
        {
            return $this->widget_name;
        }

        /**
         * Gets the title of the widget
         *
         * @since    1.0.1
         * @access   public
         * @return  string
         */
        public function get_widget_title()
        {
            return $this->widget_title;
        }

        /**
         * Gets the description of the widget
         *
         * @since    1.0.1
         * @access   public
         * @return  string
         */
        public function get_widget_description()
        {
            return $this->widget_description;
        }

        /**
         * Gets the global variable name
         *
         * @since    1.0.1
         * @access   public
         * @return  string
         */
        public function get_global_variable_name()
        {
            return $this->global_variable_name;
        }

        /**
         * Gets the plugin settings label
         *
         * @since    1.0.1
         * @access   public
         * @return  string
         */
        public function get_plugin_settings_label()
        {
            return $this->plugin_settings_label;
        }

        /**
         * Gets the plugin settings page label
         *
         * @since    1.0.1
         * @access   public
         * @return  string
         */
        public function get_plugin_settings_page_label()
        {
            return $this->plugin_settings_page_label;
        }

        /**
         * Gets the plugin settings page capabilities
         *
         * @since    1.3.0
         * @access   public
         * @return  string
         */
        public function get_plugin_settings_page_capabilities()
        {
            return $this->plugin_settings_page_capabilities;
        }

        /**
         * Gets the plugin settings menu label
         *
         * @since    1.3.0
         * @access   public
         * @return  string
         */
        public function get_plugin_settings_menu_label()
        {
            return $this->plugin_settings_menu_label;
        }

        /**
         * Gets the plugin settings menu label
         *
         * @since    1.3.0
         * @access   public
         * @return  string
         */
        public function get_plugin_settings_menu_location()
        {
            return $this->plugin_settings_menu_location;
        }

        /**
         * Gets the time to live (TTL) for data cached in transients
         *
         * @since    1.0.1
         * @access   public
         * @return  int
         */
        public function get_transient_ttl()
        {
            return $this->transient_ttl;
        }

        /**
         * Gets whether to enable styles for the admin page
         *
         * @since    1.1.0
         * @access   public
         * @return  bool
         */
        public function get_admin_enqueue_styles()
        {
            return $this->admin_enqueue_styles;
        }

        /**
         * Gets whether to enable scripts for the admin page
         *
         * @since    1.1.0
         * @access   public
         * @return  bool
         */
        public function get_admin_enqueue_scripts()
        {
            return $this->admin_enqueue_scripts;
        }

        /**
         * Gets whether to enable any actions for the admin page
         *
         * @since    1.2.0
         * @access   public
         * @return  bool
         */
        public function get_admin_enable_actions()
        {
            return $this->admin_enable_actions;
        }

        /**
         * Gets whether to enable default actions for the admin page
         *
         * @since    1.2.0
         * @access   public
         * @return  bool
         */
        public function get_admin_enable_default_actions()
        {
            return $this->admin_enable_default_actions;
        }

        /**
         * Gets whether to enable styles for the public display
         *
         * @since    1.1.0
         * @access   public
         * @return  bool
         */
        public function get_public_enqueue_styles()
        {
            return $this->public_enqueue_styles;
        }

        /**
         * Gets whether to enable scripts for the public display
         *
         * @since    1.1.0
         * @access   public
         * @return  bool
         */
        public function get_public_enqueue_scripts()
        {
            return $this->public_enqueue_scripts;
        }

        /**
         * Static singleton generator - comes in handy sometimes!
         *
         * This one needs to be an array of instances, as the meta classes will differ across plugins
         *
         * @since    1.0.0
         * @access   public
         * @return   mixed
         */
        static public function getInstance()
        {
            $class_name = get_called_class();
            if (!array_key_exists($class_name, self::$_instance) || array_key_exists($class_name, self::$_instance) && !is_a(self::$_instance[$class_name], $class_name)) {
                self::$_instance[$class_name] = new $class_name();
            }
            return self::$_instance[$class_name];
        }
    }
}