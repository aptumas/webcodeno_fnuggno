# Webcode Base - WordPress Plugin Framework

* Written by: James Inglis <james@webcode.no>
* License: GPLv2 or later
* License URI: http://www.gnu.org/licenses/gpl-2.0.html


## Overview

This WordPress Plugin Framework is a collection of classes intended to be used with a modified version of the WordPress Plugin Boilerplate, reducing the need to rewrite the same code for common features with every plugin.

It borrows ideas from other PHP frameworks such as Laravel, with the ability to set configuration in a central location and automate.


## Features

The plugin framework has the following features:

### General
* Config and meta information driven by single config file
* Command line helper scripts
* Built in internationalization
* Helper methods to reference namespaced WordPress settings
* Optional enqueuing of admin and public CSS and JS
* Out-of-the-box support for Gulp complication of SCSS and JS minification
* Support for multiple concurrent versions of the framework
* Packaged as installable WordPress plugin with no external dependencies

### Admin
* Auto-generation of admin settings page under selectable primary menu
* Custom action buttons tied to function callbacks

### API Access
* Wrapper for JSON and XML based API requests
* Optional parsing of 
* Automatic transient-based caching for remote calls

### Public
* Template loader supporting parents and child theme overrides

### Structure
* Register custom post types
* Register custom taxonomy types
* Register custom field groups for Advanced Custom Fields
* Register widgets
* Register shortcodes
* Register versionable custom database schema
* Register custom schedules
* Register scheduled tasks


## Roadmap

Slated features for the future include:

### Admin
* Support for multiple settings sections (currently only one)

### Public
* Further abstraction and development of widget base class

### Structure
* Register AJAX callbacks


## Usage

### Initialisation

1. Download repository to /wp-content/plugins/ and rename containing directory to an appropriate plugin name
2. Open <plugin>/config/config.ini and update all relevant information, notably in the [meta] section
3. From the command line, navigate to <plugin> and run `php bin/plugin_init.php`
4. All filenames and class names will be updated

### Versions

When incrementing the version number:

1. Update the version number in <plugin>/config/config.ini
2. From the command line, navigate to <plugin> and run `php bin/plugin_version_increment.php`
3. All docblocks will be updated and a new entry will have been added to the "== Changelog ==" section in README.txt


## Changelog

= 1.3.0 (2016-02-23) =
* Significant renaming of classes to allow loading multiple versions of the framework at the same time
* Implementation of helper scripts to assist with initializing a new plugin and doing routine tasks like updating version numbers.

= 1.2.0 (2016-02-19) =
* Refines admin custom actions setup
* Adds loading constants in the bootstrap

= 1.1.0 (2016-02-19) =
* First version to be marked property as a version!

