<?php

/**
 * Bootstrap file to load all the classes for the Webcode plugin base
 *
 * @version    1.3.2
 * @link       http://webcode.no
 * @since      1.0.0
 * @package    Webcodeno
 * @author     James Inglis <james@webcode.no>
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

if (!defined('WCWPPFW_1_3_2_LOADED')) {
    define('WCWPPFW_1_3_2_LOADED', true);

    require_once __DIR__ . '/class-webcodeno-base-meta.php'; // This needs to load first, before the abstract base for the other classes
    require_once __DIR__ . '/class-webcodeno-base.php';
    require_once __DIR__ . '/class-webcodeno-base-library.php';
    require_once __DIR__ . '/class-webcodeno-base-structure.php';
    require_once __DIR__ . '/class-webcodeno-base-apiaccess.php';
    require_once __DIR__ . '/class-webcodeno-base-admin.php';
    require_once __DIR__ . '/class-webcodeno-base-public.php';
    require_once __DIR__ . '/trait-webcodeno-base-queue.php';
}