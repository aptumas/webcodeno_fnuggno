<?php

/**
 * The base class for all other base classes - core methods available to all objects
 *
 * Abstract class - needs to be extended.
 *
 * @version    1.3.2
 * @link       http://webcode.no
 * @since      1.0.0
 * @package    Webcodeno_Base
 * @author     James Inglis <james@webcode.no>
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

if (!class_exists('Webcodeno_Base_1_3_2')) {
    abstract class Webcodeno_Base_1_3_2
    {
        /**
         * The ID of this plugin.
         *
         * @since    1.0.0
         * @access   private
         * @var      string $plugin_name The ID of this plugin.
         */
        protected $plugin_name;

        /**
         * The version of this plugin.
         *
         * @since    1.0.0
         * @access   private
         * @var      string $version The current version of this plugin.
         */
        protected $version;

        /**
         * The human readable title of this plugin.
         *
         * @since    1.0.0
         * @access   protected
         * @var      string $plugin_title The human readable title of this plugin.
         */
        protected $plugin_title;

        /**
         * Instance of Webcodeno_Meta
         *
         * @since    1.0.1
         * @access   protected
         * @var Webcodeno_Meta $meta
         */
        protected $meta;

        /**
         * Instance property to enable singleton operation
         *
         * @since    1.0.1
         * @access   protected
         * @var      array $_instance Instance property array to enable singleton operation
         */
        static protected $_instance = array();

        protected $transient_key;

        protected $transient_cache = true;

        protected $transient_flush = false;

        /**
         * Webcodeno_Base constructor. This should be overridden to load in the appropriate child class of Webcodeno_Meta.
         */
        public function __construct()
        {
            $this->meta = Webcodeno_Base_Meta::getInstance();
            $this->init();
        }

        /**
         * Function to be run once child class contructor has run
         */
        public function init()
        {
            $this->plugin_name = $this->meta->get_plugin_name();
            $this->version = $this->meta->get_version();
            $this->plugin_title = $this->meta->get_plugin_title();
            $this->transient_key = $this->get_transient_key();
        }

        /**
         * Gets the name of the plugin
         *
         * @return string
         */
        public function get_plugin_name()
        {
            return $this->plugin_name;
        }

        /**
         * Gets the version of the plugin
         *
         * @return string
         */
        public function get_version()
        {
            return $this->version;
        }

        /**
         * Gets a setting defined within this plugin - really, it's just to save adding the prefix every time!
         *
         * @param string $setting_name
         * @param mixed|string $default
         * @return mixed
         */
        public function get_setting($setting_name, $default = '')
        {
            return get_option($this->plugin_name . '_' . $setting_name, $default);
        }

        /**
         * Sets a setting defined within this plugin - really, it's just to save adding the prefix every time!
         *
         * @param string $setting_name
         * @param mixed $value
         * @return bool
         */
        public function update_setting($setting_name, $value)
        {
            return update_option($this->plugin_name . '_' . $setting_name, $value);
        }

        /**
         * Check whether a setting (option) exists or not
         *
         * @param string $setting_name
         * @return bool
         */
        public function check_setting_exists($setting_name)
        {
            if ($this->get_setting($setting_name, null) === null) {
                return false;
            }
            return true;
        }

        public function get_transient_key()
        {
            if ($this->check_setting_exists('transient_key') === false) {
                $this->update_transient_key();
            } elseif (isset($this->transient_key) === false) {
                $this->transient_key = $this->get_setting('transient_key');
            }
            return $this->transient_key;
        }

        public function update_transient_key()
        {
            $this->transient_key = substr(str_shuffle(md5(time())), 0, 6); // Regenerate the transient key to invalidate the cache
            $this->update_setting('transient_key', $this->transient_key);
        }

        protected function get_transient_name($property_name)
        {
            return $this->add_keys_to_name($this->plugin_name . '-' . $this->transient_key . '-' . $property_name);
        }

        public function flush_transients()
        {
            $this->transient_flush = true;
            $this->update_transient_key();
        }

        protected function get_transient_if_enabled($property_name)
        {
            if ($this->transient_cache === true && $this->transient_flush === false && get_transient($this->get_transient_name($property_name)) !== false) {
                return get_transient($this->get_transient_name($property_name));
            }
            return false;
        }

        protected function set_transient_if_enabled($property_name, $data)
        {
            if ($this->transient_cache === true) {
                set_transient($this->get_transient_name($property_name), $data, $this->meta->get_transient_ttl());
                return true;
            }
            return false;
        }

        /**
         * Use to bypass the transient caching system
         *
         * @param bool $status Set true to disable caching, false to re-enable afterwards
         * @return bool
         */
        protected function set_bypass_transient($status = true)
        {
            $this->transient_cache = !$status;
            return !$status;
        }

        public function get_path_to_this()
        {
            $rc = new ReflectionClass(get_class($this));
            return trailingslashit(dirname($rc->getFileName()));
        }

        /**
         * Gets the absolute path to the plugin
         *
         * @since  1.3.2
         * @access public
         * @return string
         */
        public function get_plugin_absolute_path()
        {
            return trailingslashit(dirname(dirname(__FILE__)));
        }

        /**
         * Gets the web path
         *
         * @since  1.3.2
         * @access public
         * @return string
         */
        public function get_plugin_uri_path()
        {
            return trailingslashit(plugins_url('', dirname(__FILE__)));
        }

        /**
         * Returns whether or not WP_DEBUG is set
         *
         * @since  1.3.1
         * @access public
         * @return bool
         */
        public function is_global_debug_set()
        {
            if (defined('WP_DEBUG') && WP_DEBUG === true) {
                return true;
            }
            return false;
        }

        /**
         * Static singleton generator - comes in handy sometimes!
         *
         * This one needs to be an array of instances, as the meta classes will differ across plugins
         *
         * @since    1.0.0
         * @access   public
         * @return   mixed
         */
        static public function getInstance()
        {
            $class_name = get_called_class();
            if (!array_key_exists($class_name, self::$_instance) || array_key_exists($class_name, self::$_instance) && !is_a(self::$_instance[$class_name], $class_name)) {
                self::$_instance[$class_name] = new $class_name();
            }
            return self::$_instance[$class_name];
        }
    }
}